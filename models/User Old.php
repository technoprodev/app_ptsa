<?php
namespace app_ptsa\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\AttributeBehavior;

class User extends \technosmart\yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'access_token' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    \common\yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'access_token'
                ],
                'value' => Yii::$app->getSecurity()->generateRandomString(40)
            ],
            /*'status' => [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    \common\yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'status'
                ],
                'value' => 1
            ]*/
        ];
    }    

    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['username', 'email'], 'unique'],
            [['username', 'email', 'password', 'repassword'], 'trim'],

            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/'],
            ['username', 'string', 'min' => 3, 'max' => 30],
            ['email', 'string', 'max' => 100],
            ['email', 'email'],
            ['no_hp', 'match', 'pattern' => '/^[0-9]+$/', 'message' => 'No Hp tidak valid.'],

            [['password', 'repassword'], 'required', 'on' => ['admin-create', 'change-password']],
            [['password', 'repassword'], 'string', 'min' => 6, 'max' => 30],
            ['repassword', 'compare', 'compareAttribute' => 'password'],
            ['oldPassword', 'required', 'on' => 'change-password'],
            ['oldPassword', 'validateOldPassword', 'on' => 'change-password'],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_INACTIVE, self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['name', 'string', 'max' => 255],

            ['id_department', 'string', 'max' => 255],
            [['id_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['id_department' => 'id']],

            // exist
        ];
    }

    public function validateOldPassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if (!$this->validatePassword($this->oldPassword)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord || (!$this->isNewRecord && $this->validate() && $this->password)) {
                $this->setPassword($this->password);
                $this->generateAuthKey();
                $this->generatePasswordResetToken();
            }
            return true;
        }
        return false;
    }

    // ambil seluruh data role model ini dari auth_assignment
    public function loadUserRoles(){
        $models = Yii::$app->authManager->getRolesByUser($this->id);
        foreach($models as $model) {
            $this->_roles[] = $model->name;
        }
    }

    public function getIdDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'id_department']);
    }
}
