<?php
namespace app_ptsa\models;

use Yii;

class User extends \technosmart\models\User
{

    public function getUser()
    {
        return $this->hasOne(UserApp::className(), ['id' => 'id']);
    }
}