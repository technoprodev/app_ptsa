<?php
namespace app_ptsa\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "poll".
 *
 * @property integer $id
 * @property string $id_department
 * @property integer $value
 * @property integer $created_at
 *
 * @property Department $department
 */
class Poll extends \technosmart\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poll';
    }

    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //id_department
            [['id_department'], 'string', 'max' => 255],
            [['id_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['id_department' => 'id']],

            //value
            [['value'], 'integer'],
            
            //created_at
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_department' => 'Id Department',
            'value' => 'Value',
            'created_at' => Yii::t('app', 'Waktu Input'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'id_department']);
    }
}
