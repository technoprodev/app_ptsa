<?php
namespace app_ptsa\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $id_department
 * @property string $no_hp
 *
 * @property Tamu[] $tamus
 * @property TamuCard[] $tamuCards
 * @property TiketEskalasi[] $tiketEskalasis
 * @property TiketEskalasi[] $tiketEskalasis0
 * @property TiketMediasi[] $tiketMediasis
 * @property Department $department
 * @property Department $ditjen
 */
class UserApp extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id

            //username
            [['username'], 'required'],
            [['username'], 'string', 'max' => 255],
            [['username'], 'unique'],
            [['username'], 'match', 'pattern' => '/^(?=.*[a-z])([a-z0-9._-]+)$/i', 'message' => 'Username at least contain 1 word. And only number, letter, dot, dashed and underscore allowed.'],

            //id_department
            [['id_department'], 'string', 'max' => 255],
            [['id_department'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['id_department' => 'id']],

            //no_hp
            [['no_hp'], 'string', 'max' => 13],
            [['no_hp'], 'match', 'pattern' => '/^(.*[0-9]){10}$/i', 'message' => 'Phone number at least contain 10 numbers. And only number is allowed.'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'id_department' => 'Department',
            'no_hp' => 'No Hp',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTamus()
    {
        return $this->hasMany(Tamu::className(), ['accepted_by_p0' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTamuCards()
    {
        return $this->hasMany(TamuCard::className(), ['accepted_by_p0' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiketEskalasis()
    {
        return $this->hasMany(TiketEskalasi::className(), ['accepted_by_p1' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiketEskalasis0()
    {
        return $this->hasMany(TiketEskalasi::className(), ['accepted_by_p2' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiketMediasis()
    {
        return $this->hasMany(TiketMediasi::className(), ['pic' => 'username']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['id' => 'id_department']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDitjen()
    {
        if ($this->department->parent == null)
            return $this->department;
        else
            return $this->department->parent0;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\technosmart\models\User::className(), ['id' => 'id']);
    }
}
