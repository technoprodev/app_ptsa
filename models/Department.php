<?php
namespace app_ptsa\models;

use Yii;

/**
 * This is the model class for table "department".
 *
 * @property string $id
 * @property string $name
 * @property string $alias
 * @property string $description
 * @property string $parent
 *
 * @property Department $parent0
 * @property Department[] $departments
 * @property TiketEskalasi[] $tiketEskalasis
 * @property TiketEskalasi[] $tiketEskalasis0
 * @property UserApp[] $users
 */
class Department extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'department';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 255],

            //name
            [['name'], 'string', 'max' => 255],

            //alias
            [['alias'], 'string', 'max' => 255],

            //description
            [['description'], 'string', 'max' => 255],

            //parent
            [['parent'], 'string', 'max' => 255],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Code',
            'name' => 'Name',
            'alias' => 'Alias',
            'description' => 'Description',
            'parent' => 'Parent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Department::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartments()
    {
        return $this->hasMany(Department::className(), ['parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiketEskalasis()
    {
        return $this->hasMany(TiketEskalasi::className(), ['p1_by_p0' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTiketEskalasis0()
    {
        return $this->hasMany(TiketEskalasi::className(), ['p2_by_p1' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(UserApp::className(), ['id_department' => 'id']);
    }
}
