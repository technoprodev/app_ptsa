<?php
namespace app_ptsa\models;

use Yii;

/**
 * This is the model class for table "tamu_card".
 *
 * @property string $no_request
 * @property string $nik
 * @property string $nama
 * @property string $no_kartu_keluarga
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $golongan_darah
 * @property string $alamat
 * @property string $rt
 * @property string $rw
 * @property string $desa_kelurahan
 * @property string $kecamatan
 * @property string $kota_kabupaten
 * @property string $provinsi
 * @property string $agama
 * @property string $pekerjaan
 * @property string $keperluan
 * @property string $no_hp
 * @property string $accepted_by_p0
 * @property string $accepted_time_p0
 * @property string $comment_by_p0
 *
 * @property UserApp $acceptedByP0
 */
class TamuCard extends \technosmart\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tamu_card';
    }

    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_request', 'nik', 'nama', 'alamat'], 'required'],
            [['tanggal_lahir', 'accepted_time_p0'], 'safe'],
            [['no_request', 'nik', 'nama', 'no_kartu_keluarga', 'tempat_lahir', 'jenis_kelamin', 'golongan_darah', 'alamat', 'rt', 'rw', 'desa_kelurahan', 'kecamatan', 'kota_kabupaten', 'provinsi', 'agama', 'pekerjaan', 'keperluan', 'no_hp', 'accepted_by_p0', 'comment_by_p0'], 'string', 'max' => 255],
            [['accepted_by_p0'], 'exist', 'skipOnError' => true, 'targetClass' => UserApp::className(), 'targetAttribute' => ['accepted_by_p0' => 'username']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_request' => 'No Request',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'no_kartu_keluarga' => 'No Kartu Keluarga',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'golongan_darah' => 'Golongan Darah',
            'alamat' => 'Alamat',
            'rt' => 'Rt',
            'rw' => 'Rw',
            'desa_kelurahan' => 'Desa Kelurahan',
            'kecamatan' => 'Kecamatan',
            'kota_kabupaten' => 'Kota Kabupaten',
            'provinsi' => 'Provinsi',
            'agama' => 'Agama',
            'pekerjaan' => 'Pekerjaan',
            'keperluan' => 'Keperluan',
            'no_hp' => 'No Hp',
            'accepted_by_p0' => 'Accepted By P0',
            'accepted_time_p0' => 'Accepted Time P0',
            'comment_by_p0' => 'Comment By P0',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedByP0()
    {
        return $this->hasOne(UserApp::className(), ['username' => 'accepted_by_p0']);
    }
}
