<?php
namespace app_ptsa\models;

use Yii;

class Monitoring extends \yii\base\Model
{
    public $bulanMulai;
    public $tahunMulai;
    public $bulanAkhir;
    public $tahunAkhir;

    public function __construct ()
    {
        $this->bulanMulai = '1';
        $this->tahunMulai = date('Y');
        $this->bulanAkhir = date('m');
        $this->tahunAkhir = date('Y');
    }
}
