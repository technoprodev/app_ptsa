<?php
namespace app_ptsa\models;

use Yii;

/**
 * This is the model class for table "tiket_eskalasi".
 *
 * @property string $no_request
 * @property string $p1_by_p0
 * @property string $reassign_time_p0
 * @property string $accepted_by_p1
 * @property string $accepted_time_p1
 * @property string $comment_by_p1
 * @property string $p2_by_p1
 * @property string $reassign_time_p1
 * @property string $reject_by_p1
 * @property string $reject_time_p1
 * @property string $reject_comment_by_p1
 * @property string $accepted_by_p2
 * @property string $accepted_time_p2
 * @property string $comment_by_p2
 * @property string $due_date_by_p2
 * @property string $reject_by_p2
 * @property string $reject_time_p2
 * @property string $reject_comment_by_p2
 * @property string $finish_comment_p2
 * @property string $finish_date_p2
 * @property string $finish_file_p2
 * @property string $cancel_by_p0
 * @property string $cancel_time_p0
 * @property string $cancel_comment_p0
 *
 * @property Department $p1ByP0
 * @property Department $p2ByP1
 * @property Tamu $noRequest
 * @property UserApp $acceptedByP1
 * @property UserApp $acceptedByP2
 * @property UserApp $cancelByP0
 */
class TiketEskalasi extends \technosmart\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiket_eskalasi';
    }

    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_request'], 'required'],
            [['reassign_time_p0', 'accepted_time_p1', 'reassign_time_p1', 'reject_time_p1', 'accepted_time_p2', 'due_date_by_p2', 'reject_time_p2', 'finish_date_p2', 'cancel_time_p0'], 'safe'],
            [['no_request', 'p1_by_p0', 'accepted_by_p1', 'comment_by_p1', 'p2_by_p1', 'reject_by_p1', 'reject_comment_by_p1', 'accepted_by_p2', 'comment_by_p2', 'reject_by_p2', 'reject_comment_by_p2', 'finish_comment_p2', 'finish_file_p2', 'cancel_by_p0', 'cancel_comment_p0'], 'string', 'max' => 255],
            [['p1_by_p0'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['p1_by_p0' => 'id']],
            [['p2_by_p1'], 'exist', 'skipOnError' => true, 'targetClass' => Department::className(), 'targetAttribute' => ['p2_by_p1' => 'id']],
            [['no_request'], 'exist', 'skipOnError' => true, 'targetClass' => Tamu::className(), 'targetAttribute' => ['no_request' => 'no_request']],
            [['accepted_by_p1'], 'exist', 'skipOnError' => true, 'targetClass' => UserApp::className(), 'targetAttribute' => ['accepted_by_p1' => 'username']],
            [['accepted_by_p2'], 'exist', 'skipOnError' => true, 'targetClass' => UserApp::className(), 'targetAttribute' => ['accepted_by_p2' => 'username']],
            [['cancel_by_p0'], 'exist', 'skipOnError' => true, 'targetClass' => UserApp::className(), 'targetAttribute' => ['cancel_by_p0' => 'username']],
            [['p1_by_p0'], 'required', 'on' => ['eskalasi']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_request' => 'No Request',
            'p1_by_p0' => 'Unit P1',
            'reassign_time_p0' => 'Waktu Petugas Loket Reassign',
            'accepted_by_p1' => 'P1',
            'accepted_time_p1' => 'Waktu P1 Selesai',
            'comment_by_p1' => 'Keterangan P1',
            'p2_by_p1' => 'Unit P2',
            'reassign_time_p1' => 'Waktu P1 Reassign',
            'reject_by_p1' => 'P1 yang me-reject',
            'reject_time_p1' => 'Waktu P1 reject tiket',
            'reject_comment_by_p1' => 'Alasan P1 reject tiket',
            'accepted_by_p2' => 'PIC P2',
            'accepted_time_p2' => 'Waktu P2 mulai',
            'comment_by_p2' => 'Keterangan P2',
            'due_date_by_p2' => 'Due Date',
            'reject_by_p2' => 'P2 yang me-reject',
            'reject_time_p2' => 'Waktu P2 reject tiket',
            'reject_comment_by_p2' => 'Alasan P2 reject tiket',
            'finish_comment_p2' => 'Keterangan P2 selesai',
            'finish_date_p2' => 'Waktu P2 selesai',
            'finish_file_p2' => 'Dokumen pemohon',
            'cancel_by_p0' => 'P0 yang cancel',
            'cancel_time_p0' => 'Waktu P0 cancel tiket',
            'cancel_comment_p0' => 'Alasan P0 cancel tiket',
        ];
    }

    public function attributeHints()
    {
        return [
            'due_date_by_p2' => 'cth: 26/08/2017',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getP1ByP0()
    {
        return $this->hasOne(Department::className(), ['id' => 'p1_by_p0']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getP2ByP1()
    {
        return $this->hasOne(Department::className(), ['id' => 'p2_by_p1']);
    }

    public function setP1ByP0($value)
    {
        return $this->p1ByP0 = $value;
    }

    public function setP2ByP1($value)
    {
        return $this->p2ByP1 = $value;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoRequest()
    {
        return $this->hasOne(Tamu::className(), ['no_request' => 'no_request']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedByP1()
    {
        return $this->hasOne(UserApp::className(), ['username' => 'accepted_by_p1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcceptedByP2()
    {
        return $this->hasOne(UserApp::className(), ['username' => 'accepted_by_p2']);
    }

    public function setAcceptedByP1($value)
    {
        $this->acceptedByP1 = $value;
    }

    public function setAcceptedByP2($value)
    {
        $this->acceptedByP2 = $value;
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getRejectByP0()
    {
        return $this->hasOne(UserApp::className(), ['username' => 'reject_by_p0']);
    }

    public function setRejectByP0($value)
    {
        $this->acceptedByP2 = $value;
    }

    public function getStateStatus()
    {
        if($this->p1_by_p0 != null && $this->p1_by_p0 != \Yii::$app->user->identity->user->id_department && $this->accepted_by_p1 == null)
            return [ 1, 'Tiket ini belum dikerjakan oleh tim ' . $this->p1ByP0->name];

        if($this->p1_by_p0 != null && $this->p1_by_p0 == \Yii::$app->user->identity->user->id_department && $this->accepted_by_p1 == null) //kl accepted_by_p1 tidak null, berarti p2_by_p1 nya juga tidak null
            return [ 2, 'Tiket ini belum dikerjakan oleh tim P1 Anda'];


        if($this->p2_by_p1 != null && $this->p2_by_p1 != \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 == null)
            return [ 3, 'Tiket ini belum dikerjakan oleh tim ' . $this->p2ByP1->name];

        if($this->p2_by_p1 != null && $this->p2_by_p1 != \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 != null && $this->accepted_by_p2 != \Yii::$app->user->identity->user->username && $this->finish_date_p2 == null)
            return [ 4, 'Tiket ini sedang dikerjakan oleh tim ' . $this->p2ByP1->name];

        if($this->p2_by_p1 != null && $this->p2_by_p1 != \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 != null && $this->accepted_by_p2 != \Yii::$app->user->identity->user->username && $this->finish_date_p2 != null)
            return [ 5, 'Tiket ini sudah selesai dikerjakan oleh tim ' . $this->p2ByP1->name];


        if($this->p2_by_p1 != null && $this->p2_by_p1 == \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 == null)
            return [ 6, 'Tiket ini belum dikerjakan oleh tim P2 Anda'];

        if($this->p2_by_p1 != null && $this->p2_by_p1 == \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 != null && $this->accepted_by_p2 != \Yii::$app->user->identity->user->username && $this->finish_date_p2 == null)
            return [ 7, 'Tiket ini sedang dikerjakan oleh tim P2 Anda'];

        if($this->p2_by_p1 != null && $this->p2_by_p1 == \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 != null && $this->accepted_by_p2 == \Yii::$app->user->identity->user->username && $this->finish_date_p2 == null)
            return [ 8, 'Tiket ini sedang anda kerjakan'];
        
        if($this->p2_by_p1 != null && $this->p2_by_p1 == \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 != null && $this->accepted_by_p2 != \Yii::$app->user->identity->user->username && $this->finish_date_p2 != null)
            return [ 9, 'Tiket ini sudah selesai dikerjakan tim P2 Anda'];

        if($this->p2_by_p1 != null && $this->p2_by_p1 == \Yii::$app->user->identity->user->id_department && $this->accepted_by_p2 != null && $this->accepted_by_p2 == \Yii::$app->user->identity->user->username && $this->finish_date_p2 != null)
            return [ 10, 'Tiket ini sudah anda kerjakan'];
    }
}
