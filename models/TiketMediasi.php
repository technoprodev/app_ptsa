<?php
namespace app_ptsa\models;

use Yii;

/**
 * This is the model class for table "tiket_mediasi".
 *
 * @property string $no_request
 * @property string $pic
 * @property string $no_hp
 *
 * @property Tamu $noRequest
 * @property UserApp $pic0
 */
class TiketMediasi extends \technosmart\yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiket_mediasi';
    }

    public static function getDb()
    {
        return Yii::$app->get('dba');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_request'], 'required'],
            [['no_hp'], 'safe'],
            [['no_request', 'pic', 'no_hp'], 'string', 'max' => 255],
            [['no_request'], 'exist', 'skipOnError' => true, 'targetClass' => Tamu::className(), 'targetAttribute' => ['no_request' => 'no_request']],
            [['pic'], 'exist', 'skipOnError' => true, 'targetClass' => UserApp::className(), 'targetAttribute' => ['pic' => 'username']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_request' => 'No Request',
            'pic' => 'PIC Mediasi',
            'no_hp' => 'No HP Revisi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoRequest()
    {
        return $this->hasOne(Tamu::className(), ['no_request' => 'no_request']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPic0()
    {
        return $this->hasOne(UserApp::className(), ['username' => 'pic']);
    }
}
