$(document).ready(function() {
	$("#sortable ul").sortable({
        connectWith: "#sortable ul",
        placeholder: "ui-state-highlight",
		handle: ".sortable-handle",
		start: function(e, ui) {
			$("body, html").addClass("ui-sortable-handle");
		},
		stop: function(e, ui) {
			$("body, html").removeClass("ui-sortable-handle");
		},
		over: function(e, ui) {
			ui.placeholder.html(ui.item.html());
		},
		update: function(e, ui) {
			// prevent for repetition
			if (this === ui.item.parent()[0]) {
				$("#sortable .loading").fadeIn();

				$.ajax({
					url: url.to("menu/position"),
					method: "post",
					data: {
						parent: ui.item.parents("li:first").data("id") || null,
						order: ui.item.index(),
						id: ui.item.data("id"),
					},
					complete: function(xhr, status) {
						$("#sortable .loading").fadeOut();
					},
					error: function() {
						fn.alertError();
					}
				});
			}
		},
	})
	.disableSelection();
});