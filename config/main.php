<?php
$params['user.passwordResetTokenExpire'] = 3600;

$config = [
    'id' => 'app_ptsa',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app_ptsa\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-ptsa',
        ],
        'user' => [
            'identityClass' => 'app_ptsa\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-ptsa', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'session-ptsa',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];

return $config;