<?php

use app_ptsa\assets_manager\RequiredAsset;
use yii\helpers\Html;
use technosmart\yii\widgets\Menu as MenuWidget;
use yii\widgets\Breadcrumbs;
use technosmart\yii\widgets\Alert;
use technosmart\models\Menu;

RequiredAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="x-ua-compatible" content="ie=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="description" content="<?= Yii::$app->params['app.description'] ?>">
        <meta name="keywords" content="<?= Yii::$app->params['app.keywords'] ?>">
        <meta name="author" content="<?= Yii::$app->params['app.author'] ?>">
        <?= Html::csrfMetaTags() ?>
        <meta name="base-url" content="<?= yii\helpers\Url::home(true) ?>">
        <title><?= $this->title ? strip_tags($this->title) . ' | ' : null ?><?= Yii::$app->params['app.name'] ?><?= Yii::$app->params['app.description'] ? ' - ' . Yii::$app->params['app.description'] : null ?></title>
        <link rel="shortcut icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/favicon.ico">
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- START @ALERT & CONFIRM -->
        <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-labelledby="title-alert">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-alert"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="title-confirm">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="title-confirm"></h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default modal-yes" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-default modal-no" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /END @ALERT & CONFIRM -->

        <!-- START @WRAPPER -->
        <section id="wrapper">
            <!-- START @HEADER -->
            <header id="header" class="border-bottom padding-y-10">
                <div class="fs-18 pull-left margin-left-20" style="width: 230px;">
                    <a href="<?= Yii::$app->urlManager->createUrl("site/index") ?>" class="underline-none">
                        <span class="text-dark-azure f-italic">kemnaker</span><span class="f-normal margin-x-5 bg-azure padding-x-5 padding-y-5 rounded-sm">PTSA</span>
                    </a>
                </div>
                <div class="pull-right margin-right-20"><a class="underline-none rounded-xs padding-x-10 padding-y-5 border-light-azure hover-border-light-azure hover-bg-light-azure" data-method="post" href="<?= Yii::$app->urlManager->createUrl("site/logout") ?>">Logout</a></div>
                <div class="pull-right margin-right-20"><a class="underline-none rounded-xs padding-x-10 padding-y-5 border-lighter hover-border-light-azure hover-bg-light-azure" data-method="post" href="<?= Yii::$app->urlManager->createUrl("polling/index") ?>">Polling</a></div>
                <div class="pull-right margin-right-20"><a class="underline-none rounded-xs padding-x-10 padding-y-5 border-lighter hover-border-light-azure hover-bg-light-azure" data-method="post" href="http://192.168.71.50/sisan_v3">Web Antrian</a></div>
            </header>
            <!-- /END @HEADER -->

            <!-- START @BODY -->
            <section id="body" class="has-sidebar-left">
                <!-- START @SIDEBAR LEFT -->
                <aside id="sidebar-left" class="border-right padding-top-15">
                    <div>
                        <div class="box box-break-sm padding-x-10">
                            <div class="box-3 padding-x-0">
                                <i class="fa fa-user fs-26 circle-icon border-light-azure bg-light-azure text-azure"></i>
                            </div>
                            <div class="box-9 padding-x-0">
                                <div class="f-bold text-grayest">
                                    <?= \Yii::$app->user->identity->name; ?>
                                </div>
                                <div class="">
                                    <?= \Yii::$app->user->identity->email; ?>
                                </div>
                            </div>
                        </div>
                        <div class="padding-x-10 margin-top-15">
                            <?php $roles; foreach (\Yii::$app->authManager->getRolesByUser(\Yii::$app->user->identity->getId()) as $key => $value) : ?>
                                <?php $roles[] = '<span class="bg-light-azure padding-y-2 padding-x-5 rounded-sm">' . $value->description . '</span>' ?>
                            <?php endforeach; ?>
                            Role : <?= isset($roles) ? implode('&nbsp;&nbsp;', $roles) : '<span class="f-italic">(kosong)</span>'?>
                        </div>
                    </div>
                    <?php
                        $cache = Yii::$app->cache;
                        $key = 'menu-ptsa-sidebar';
                        if ($cache->exists($key)) {
                            $cacheData = $cache->get($key);
                        } else {
                            $cacheData = [
                                'p0' => technosmart\models\Menu::find()->where(['code' => 'ptsa-p0'])->asArray()->all(),
                                'p1' => technosmart\models\Menu::find()->where(['code' => 'ptsa-p1'])->asArray()->all(),
                                'p2' => technosmart\models\Menu::find()->where(['code' => 'ptsa-p2'])->asArray()->all(),
                                'monitoring' => technosmart\models\Menu::find()->where(['code' => 'ptsa-monitoring'])->asArray()->all(),
                                'admin' => technosmart\models\Menu::find()->where(['code' => 'ptsa-admin'])->asArray()->all(),
                            ];
                            $cache->set($key, $cacheData);
                        }
                    ?>

                    <?php 
                        $menuP0 = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['p0'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bold submenu-active-bold menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                                'id' => 'menu-y',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                        if ($menuP0): ?>
                        <hr class="margin-y-15">
                        <div class="f-bold padding-left-15 fs-13">Menu P0</div>
                        <?= $menuP0 ?>
                    <?php endif; ?>

                    <?php 
                        $menuP1 = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['p1'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                                'id' => 'menu-y',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                        if ($menuP1): ?>
                        <hr class="margin-y-15">
                        <div class="f-bold padding-left-15 fs-13">Menu P1</div>
                        <?= $menuP1 ?>
                    <?php endif; ?>

                    <?php 
                        $menuP2 = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['p2'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                                'id' => 'menu-y',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                        if ($menuP2): ?>
                        <hr class="margin-y-15">
                        <div class="f-bold padding-left-15 fs-13">Menu P2</div>
                        <?= $menuP2 ?>
                    <?php endif; ?>

                    <?php 
                        $menuMonitoring = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['monitoring'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                                'id' => 'menu-y',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                        if ($menuMonitoring): ?>
                        <hr class="margin-y-15">
                        <div class="f-bold padding-left-15 fs-13">Monitoring</div>
                        <?= $menuMonitoring ?>
                    <?php endif; ?>

                    <?php 
                        $menuAdmin = MenuWidget::widget([
                            'items' => Menu::menuTree($cacheData['admin'], null, true),
                            'options' => [
                                'class' => 'menu-y menu-xs menu-active-bg-light-blue menu-hover-darker-10 submenu-active-bg-light-blue',
                                'id' => 'menu-y',
                            ],
                            'activateItems' => true,
                            'openParents' => true,
                            'parentsCssClass' => 'has-submenu',
                            'encodeLabels' => false,
                            'labelTemplate' => '<a>{label}</a>',
                            'hideEmptyItems' => true,
                        ]);
                        if ($menuAdmin): ?>
                        <hr class="margin-y-15">
                        <div class="f-bold padding-left-15 fs-13">Admin</div>
                        <?= $menuAdmin ?>
                    <?php endif; ?>
                </aside>
                <!-- /END @SIDEBAR LEFT -->

                <!-- START @PAGE WRAPPER -->
                <section id="page-wrapper">
                    <!-- START @PAGE SLIDER -->
                    <!-- <section id="page-slider">
                    </section> -->
                    <!-- /END @PAGE SLIDER -->

                    <!-- START @PAGE HEADER -->
                    <section id="page-header" class="border-bottom border-lighter">
                        <div class="fs-20"><?= Html::decode($this->title) ?></div>
                    </section>
                    <!-- /END @PAGE HEADER -->

                    <!-- START @PAGE BODY -->
                    <section id="page-body">
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </section>
                    <!-- /END @PAGE BODY -->
                </section>
                <!-- /END @PAGE WRAPPER -->

                <!-- START @SIDEBAR RIGHT -->
                <!-- <aside id="sidebar-right" class="border-left">
                </aside> -->
                <!-- /END @SIDEBAR RIGHT -->
            </section>
            <!-- /END @BODY -->

            <!-- START @FOOTER -->
            <footer id="footer" class="border-top bg-lighter">
                <div class="container text-center fs-18">KEMNAKER © <?= date('Y') ?></div>
            </footer>
            <!-- /END @FOOTER -->
        </section>
        <!-- /END @WRAPPER -->

        <!-- START @BACK TO TOP -->
        <!-- <div id="back-to-top">
            <div class="padding-10 bg-gray fs-18 border">
                <span class="glyphicon glyphicon-arrow-up"></span>
            </div>
        </div> -->
        <!-- /END @BACK TO TOP -->

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>