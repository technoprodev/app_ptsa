<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<div class="margin-top-30 margin-left-20">
    <?php $form = ActiveForm::begin(); ?>
        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
        <div class="box box-break-sm">
            <div class="box-2 padding-left-0">
                <?= $form->field($model['monitoring'], 'bulanMulai')->begin(); ?>
                    <?= Html::activeLabel($model['monitoring'], 'bulanMulai', ['class' => 'control-label']); ?>
                    <?= Html::activeDropDownList($model['monitoring'], 'bulanMulai', [
                        '01' => 'Januari',
                        '02' => 'Februari',
                        '03' => 'Maret',
                        '04' => 'April',
                        '05' => 'Mei',
                        '06' => 'Juni',
                        '07' => 'Juli',
                        '08' => 'Agustus',
                        '09' => 'September',
                        '10' => 'Oktober',
                        '11' => 'November',
                        '12' => 'Desember',
                    ], ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
                    <?= Html::error($model['monitoring'], 'bulanMulai', ['class' => 'help-block']); ?>
                <?= $form->field($model['monitoring'], 'bulanMulai')->end(); ?>
            </div>
            <div class="box-2 padding-left-0 padding-right-40">
                <?= $form->field($model['monitoring'], 'tahunMulai')->begin(); ?>
                    <?= Html::activeLabel($model['monitoring'], 'tahunMulai', ['class' => 'control-label']); ?>
                    <?= Html::activeDropDownList($model['monitoring'], 'tahunMulai', [
                        '2016' => '2016',
                        '2017' => '2017',
                        '2018' => '2018',
                        '2019' => '2019',
                    ], ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
                    <?= Html::error($model['monitoring'], 'tahunMulai', ['class' => 'help-block']); ?>
                <?= $form->field($model['monitoring'], 'tahunMulai')->end(); ?>
            </div>
            <div class="box-2 padding-left-0">
                <?= $form->field($model['monitoring'], 'bulanAkhir')->begin(); ?>
                    <?= Html::activeLabel($model['monitoring'], 'bulanAkhir', ['class' => 'control-label']); ?>
                    <?= Html::activeDropDownList($model['monitoring'], 'bulanAkhir', [
                        '01' => 'Januari',
                        '02' => 'Februari',
                        '03' => 'Maret',
                        '04' => 'April',
                        '05' => 'Mei',
                        '06' => 'Juni',
                        '07' => 'Juli',
                        '08' => 'Agustus',
                        '09' => 'September',
                        '10' => 'Oktober',
                        '11' => 'November',
                        '12' => 'Desember',
                    ], ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
                    <?= Html::error($model['monitoring'], 'bulanAkhir', ['class' => 'help-block']); ?>
                <?= $form->field($model['monitoring'], 'bulanAkhir')->end(); ?>
            </div>
            <div class="box-2 padding-left-0 padding-right-40">
                <?= $form->field($model['monitoring'], 'tahunAkhir')->begin(); ?>
                    <?= Html::activeLabel($model['monitoring'], 'tahunAkhir', ['class' => 'control-label']); ?>
                    <?= Html::activeDropDownList($model['monitoring'], 'tahunAkhir', [
                        '2016' => '2016',
                        '2017' => '2017',
                        '2018' => '2018',
                        '2019' => '2019',
                    ], ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
                    <?= Html::error($model['monitoring'], 'tahunAkhir', ['class' => 'help-block']); ?>
                <?= $form->field($model['monitoring'], 'tahunAkhir')->end(); ?>
            </div>
            <div class="box-1 padding-left-0 padding-right-40" style="padding-top: 23px">
                <button type="submit" class="btn btn-default btn-block border-azure text-azure bg-lightest hover-bg-azure rounded-xs">Filter</button>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
</div>

<div class="margin-top-30 margin-bottom-10 margin-left-20 f-italic">
    Total dari bulan <?= date('F Y', strtotime($waktuMulai)) ?> hingga bulan <?= date('F Y', strtotime($waktuAkhir)) ?>
</div>

<div class="box box-break-sm text-center margin-left-20">
    <div class="box-3 padding-left-0 padding-right-40">
        <div class="border-azure rounded-xs">
            <div class="bg-azure padding-y-5 fs-16">Tiket Eskalasi</div>
            <div class="padding-y-30 fs-22 text-azure">
                <?= $data_count_kinerja['tiket_eskalasi'] ?> Tiket
            </div>
        </div>
    </div>
    <div class="box-3 padding-left-0 padding-right-40">
        <div class="border-azure rounded-xs">
            <div class="bg-azure padding-y-5 fs-16">Tiket Mediasi</div>
            <div class="padding-y-30 fs-22 text-azure">
                <?= $data_count_kinerja['tiket_mediasi'] ?> Tiket
            </div>
        </div>
    </div>
    <div class="box-3 padding-left-0 padding-right-40">
        <div class="border-azure rounded-xs">
            <div class="bg-azure padding-y-5 fs-16">Tiket Selesai di Tempat</div>
            <div class="padding-y-30 fs-22 text-azure">
                <?= $data_count_kinerja['tiket_selesai'] ?> Tiket
            </div>
        </div>
    </div>
</div>

<div class="margin-top-50 margin-bottom-20 margin-left-20 f-italic">
    Rincian
</div>

<?php foreach ($data_kinerja as $key => $value) : ?>
    <div class="margin-left-20 margin-bottom-10 f-bold">
        <?= $value['name'] ?>
    </div>
    <div class="box box-break-sm text-center margin-bottom-30 margin-left-20">
        <div class="box-2 padding-left-0 padding-right-40">
            <div class="border-azure rounded-xs">
                <div class="bg-azure padding-y-2">Tiket Eskalasi</div>
                <div class="padding-y-20 fs-22 text-azure">
                    <?= $value['tiket_eskalasi'] ?> Tiket
                </div>
            </div>
        </div>
        <div class="box-2 padding-left-0 padding-right-40">
            <div class="border-azure rounded-xs">
                <div class="bg-azure padding-y-2">Tiket Mediasi</div>
                <div class="padding-y-20 fs-22 text-azure">
                    <?= $value['tiket_mediasi'] ?> Tiket
                </div>
            </div>
        </div>
        <div class="box-2 padding-left-0 padding-right-40">
            <div class="border-azure rounded-xs">
                <div class="bg-azure padding-y-2">Tiket Selesai di Tempat</div>
                <div class="padding-y-20 fs-22 text-azure">
                    <?= $value['tiket_selesai'] ?> Tiket
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>