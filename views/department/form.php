<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Department';
$this->params['breadcrumbs'][] = $this->title;

technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-70 m-margin-left-0">
    <div class="col-xs-6">    
<?php endif; ?>
    
    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app', 'class' => 'margin-bottom-40 margin-top-20']]); ?>

    	<div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['department'], 'id')->begin(); ?>
                    <?= Html::activeLabel($model['department'], 'id', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['department'], 'id', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['department'], 'id', ['class' => 'help-block']); ?>
                <?= $form->field($model['department'], 'id')->end(); ?>
            </div>

            <div class="box-6 padding-right-0">
                <?= $form->field($model['department'], 'alias')->begin(); ?>
                    <?= Html::activeLabel($model['department'], 'alias', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['department'], 'alias', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['department'], 'alias', ['class' => 'help-block']); ?>
                <?= $form->field($model['department'], 'alias')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['department'], 'name')->begin(); ?>
                    <?= Html::activeLabel($model['department'], 'name', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['department'], 'name', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['department'], 'name', ['class' => 'help-block']); ?>
                <?= $form->field($model['department'], 'name')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['department'], 'parent')->begin(); ?>
	                <?= Html::activeLabel($model['department'], 'parent', ['class' => 'control-label']); ?>
	                <?= Html::activeDropDownList(
	                    $model['department'],
	                    'parent',
	                    ArrayHelper::map(
	                        // (new \yii\db\Query())->select("id, name")->from("department")->where("parent IS NULL")->orderby("name")->all(),
	                        \Yii::$app->dba->createCommand(
	                            "SELECT d.id, d.name FROM department d
	                            WHERE
	                                d.parent IS NULL"
	                        )->queryAll(),
	                        'id',
	                        'name'
	                    ),
	                    [
	                        'class' => 'form-control select2',
	                        'prompt' => '-- Pilih Data --',
	                        'maxlength' => true,
	                    ]
	                ); ?>
	                <?= Html::error($model['department'], 'parent', ['class' => 'help-block']); ?>
	            <?= $form->field($model['department'], 'parent')->end(); ?>
            </div>
        </div>

        <?= $form->field($model['department'], 'description')->begin(); ?>
            <?= Html::activeLabel($model['department'], 'description', ['class' => 'control-label']); ?>
            <?= Html::activeTextArea($model['department'], 'description', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
            <?= Html::error($model['department'], 'description', ['class' => 'help-block']); ?>
        <?= $form->field($model['department'], 'description')->end(); ?>

<div class="form-group">
    <?= Html::submitButton($model['department']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
    <?= Html::resetButton('Reset', ['class' => 'btn bg-lightest']); ?>
</div>

<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>