<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-6 margin-top-15">
<?php endif; ?>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['department']->attributeLabels()['id'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= $model['department']->id ?></div>
</div>
    
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['department']->attributeLabels()['alias'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= $model['department']->alias ? $model['department']->alias : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['department']->attributeLabels()['name'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= $model['department']->name ? $model['department']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['department']->attributeLabels()['description'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= $model['department']->description ? $model['department']->description : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['department']->attributeLabels()['parent'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= $model['department']->parent ? $model['department']->parent : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div>
            <?= Html::a('Update', ['form', 'id' => $model['department']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['department']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>