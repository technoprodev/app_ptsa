<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Penilaian Kepuasan Pelayanan';
$this->params['breadcrumbs'][] = $this->title;
?>

<img width="50" class="img-circle center-block margin-top-40 margin-bottom-40 m-margin-top-10 m-margin-bottom-10" src="<?= Yii::$app->request->baseUrl ?>/img/logo.png" alt="Logo" />
<div class="padding-x-50 m-padding-x-0">
    <div class="fs-26 margin-bottom-50 text-center text-orange f-bold">
        <span class="">Polling Kepuasan Pelayanan PTSA</span>
    </div>
    <div class="text-center margin-bottom-30">
        <div class="box box-space-lg box-break-sm">
            <a class="box-4 underline-none padding-x-50 m-padding-x-10" href="<?= Yii::$app->urlManager->createUrl(['polling/index', 'value' => '2']) ?>">
                <img height="180" class="img-rounded center-block margin-bottom-20" src="<?= Yii::$app->request->baseUrl ?>/img/2.png" alt="Puas" />
                <button class="btn btn-block bg-light-azure border-azure hover-bg-azure fs-20 margin-bottom-50">Puas</button>
            </a>
            <a class="box-4 underline-none padding-x-50 m-padding-x-10" href="<?= Yii::$app->urlManager->createUrl(['polling/index', 'value' => '1']) ?>">
                <img height="180" class="img-rounded center-block margin-bottom-20" src="<?= Yii::$app->request->baseUrl ?>/img/1.png" alt="Cukup Puas" />
                <button class="btn btn-block bg-light-spring border-spring hover-bg-spring fs-20 margin-bottom-50">Cukup Puas</button>
            </a>
            <a class="box-4 underline-none padding-x-50 m-padding-x-10" href="<?= Yii::$app->urlManager->createUrl(['polling/index', 'value' => '0']) ?>">
                <img height="180" class="img-rounded center-block margin-bottom-20" src="<?= Yii::$app->request->baseUrl ?>/img/0.png" alt="Tidak Puas" />
                <button class="btn btn-block bg-light-rose border-rose hover-bg-rose fs-20 margin-bottom-50">Belum Puas</button>
            </a>
        </div>
        <?php if (Yii::$app->session->hasFlash('poll-success')): ?>
            <h4 id="success-message" class="bg-azure padding-y-10 margin-x-80"><?= Yii::$app->session->getFlash('poll-success') ?></h4>
        <?php endif; ?>
    </div>
</div>

<?php $this->registerJs(
    '(function($) {setTimeout(function() {$("#success-message").hide(200)}, 3000);})(jQuery);',
    yii\web\View::POS_END,
    null
) ?>