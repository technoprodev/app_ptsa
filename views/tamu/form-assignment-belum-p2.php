<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Set Due Date P2 ' . (isset($model['tamu']->no_request) ? '<span class="f-bold">' . $model['tamu']->no_request . '</span>' : '');
$this->params['breadcrumbs'][] = $this->title;

technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);

$time = $model['tiket_eskalasi']->due_date_by_p2;
if (!$time) $time = date('d/m/Y');
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-70 m-margin-left-0">
    <div class="col-xs-6">    
<?php endif; ?>
    
    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app', 'class' => 'margin-bottom-40 margin-top-20']]); ?>
    
        <input class='hidden' type='text' name='AcceptTiket' value="<?=$model['accept_tiket']?>" v-model='accept_tiket' v-default-value="<?=$model['accept_tiket']?>">
                    
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['no_request'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->no_request) ? $model['tamu']->no_request : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['nik'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->nik) ? $model['tamu']->nik : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['nama'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->nama) ? $model['tamu']->nama : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['tempat_lahir'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->tempat_lahir) ? $model['tamu']->tempat_lahir : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['tanggal_lahir'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->tanggal_lahir) ? $model['tamu']->tanggal_lahir : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['jenis_kelamin'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->jenis_kelamin) ? $model['tamu']->jenis_kelamin : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['agama'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->agama) ? $model['tamu']->agama : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['no_kartu_keluarga'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->no_kartu_keluarga) ? $model['tamu']->no_kartu_keluarga : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['golongan_darah'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->golongan_darah) ? $model['tamu']->golongan_darah : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['alamat'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->alamat) ? $model['tamu']->alamat : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['rt'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->rt) ? $model['tamu']->rt : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['rw'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->rw) ? $model['tamu']->rw : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['desa_kelurahan'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->desa_kelurahan) ? $model['tamu']->desa_kelurahan : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['kecamatan'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->kecamatan) ? $model['tamu']->kecamatan : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['kota_kabupaten'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->kota_kabupaten) ? $model['tamu']->kota_kabupaten : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['provinsi'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->provinsi) ? $model['tamu']->provinsi : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['pekerjaan'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->pekerjaan) ? $model['tamu']->pekerjaan : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <hr>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['instansi'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->instansi) ? $model['tamu']->instansi : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['keperluan'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->keperluan) ? $model['tamu']->keperluan : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['no_hp'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->no_hp) ? $model['tamu']->no_hp : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <hr>

        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0">PIC P0</div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->acceptedByP0->user->name) ? $model['tamu']->acceptedByP0->user->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['comment_by_p0'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->comment_by_p0) ? $model['tamu']->comment_by_p0 : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['accepted_time_p0'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->accepted_time_p0) ? $model['tamu']->accepted_time_p0 : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0">Diteruskan Kepada</div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->p1ByP0->name) ? $model['tiket_eskalasi']->p1ByP0->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <hr>
        
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0">P1</div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->acceptedByP1->user->name) ? $model['tiket_eskalasi']->acceptedByP1->user->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['comment_by_p1'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->comment_by_p1) ? $model['tiket_eskalasi']->comment_by_p1 : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['accepted_time_p1'] ?></div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->accepted_time_p1) ? $model['tiket_eskalasi']->accepted_time_p1 : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <div class="box margin-bottom-10">
            <div class="box-2 text-right padding-x-0">Diteruskan Kepada</div>
            <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->p2ByP1->name) ? $model['tiket_eskalasi']->p2ByP1->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
        </div>
        <hr>
                    
        <div class="form-group radio-elegant">
            <label class="control-label">Tindak Lanjut</label>
            <div class="row row-radio">
                <div class="radio col-xs-3">
                    <label>
                        <input name="radio-column" value="1" type="radio" v-on:click="setAcceptTiket(1)">
                        <i></i>Accept
                    </label>
                </div>
                <div class="radio col-xs-3">
                    <label>
                        <input name="radio-column" value="2" type="radio" v-on:click="setAcceptTiket(2)">
                        <i></i>Reject
                    </label>
                </div>
            </div>
        </div>

        <div v-if="(accept_tiket==1)" v-init>
            <?= $form->field($model['tiket_eskalasi'], 'comment_by_p2')->begin(); ?>
                <?= Html::activeLabel($model['tiket_eskalasi'], 'comment_by_p2', ['class' => 'control-label']); ?>
                <?= Html::activeTextArea($model['tiket_eskalasi'], 'comment_by_p2', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
                <?= Html::error($model['tiket_eskalasi'], 'comment_by_p2', ['class' => 'help-block']); ?>
            <?= $form->field($model['tiket_eskalasi'], 'comment_by_p2')->end(); ?>

            <?= $form->field($model['tiket_eskalasi'], 'due_date_by_p2')->begin(); ?>
                <?= Html::activeLabel($model['tiket_eskalasi'], 'due_date_by_p2', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['tiket_eskalasi'], 'due_date_by_p2', ['class' => 'form-control input-mask-date input-datepicker', 'data-date-start-date' => $time, 'maxlength' => true]); ?>
                <div class="fs-11"><?= $model['tiket_eskalasi']->attributeHints()['due_date_by_p2'] ? $model['tiket_eskalasi']->attributeHints()['due_date_by_p2'] : null ?></div>
                <?= Html::error($model['tiket_eskalasi'], 'due_date_by_p2', ['class' => 'help-block']); ?>
            <?= $form->field($model['tiket_eskalasi'], 'due_date_by_p2')->end(); ?>
        </div>

        <div v-if="(accept_tiket==2)" v-init>
            <?= $form->field($model['tiket_eskalasi'], 'reject_comment_by_p2')->begin(); ?>
                <?= Html::activeLabel($model['tiket_eskalasi'], 'reject_comment_by_p2', ['class' => 'control-label']); ?>
                <?= Html::activeTextArea($model['tiket_eskalasi'], 'reject_comment_by_p2', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true]); ?>
                <?= Html::error($model['tiket_eskalasi'], 'reject_comment_by_p2', ['class' => 'help-block']); ?>
            <?= $form->field($model['tiket_eskalasi'], 'reject_comment_by_p2')->end(); ?>
        </div>
        <hr v-if = 'accept_tiket == 1 || accept_tiket == 2'>

        <?= Html::submitButton('<i class="fa fa-save margin-right-10"></i>Submit', ['class' => 'btn btn-fancy bg-azure border-azure hover-bg-light-blue', 'v-if' => 'accept_tiket!=0']) ?>

    <?php ActiveForm::end(); ?>
        
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>