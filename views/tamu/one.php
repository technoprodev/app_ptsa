<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row">
    <div class="col-xs-6 margin-top-15">
<?php endif; ?>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">Tipe Tiket</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0">
        <?php switch ($model['tipe_tiket']) {
            case 1:
                echo 'Tiket Eskalasi';
                break;
            case 2:
                echo 'Tiket Mediasi';
                break;
            case 3:
                echo 'Tiket Selesai di Tempat';
                break;
        } ?>
    </div>
</div>
<hr>

<?php if($model['tamu']->getTableType() == 1): ?>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">Status Tiket</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= $model['tiket_eskalasi']->getStateStatus()[1] ?></div>
</div>
<hr>
<?php endif; ?>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['no_request'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->no_request) ? $model['tamu']->no_request : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['nik'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->nik) ? $model['tamu']->nik : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['nama'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->nama) ? $model['tamu']->nama : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['tempat_lahir'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->tempat_lahir) ? $model['tamu']->tempat_lahir : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['tanggal_lahir'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->tanggal_lahir) ? $model['tamu']->tanggal_lahir : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['jenis_kelamin'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->jenis_kelamin) ? $model['tamu']->jenis_kelamin : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['agama'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->agama) ? $model['tamu']->agama : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['no_kartu_keluarga'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->no_kartu_keluarga) ? $model['tamu']->no_kartu_keluarga : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['golongan_darah'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->golongan_darah) ? $model['tamu']->golongan_darah : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['alamat'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->alamat) ? $model['tamu']->alamat : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['rt'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->rt) ? $model['tamu']->rt : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['rw'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->rw) ? $model['tamu']->rw : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['desa_kelurahan'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->desa_kelurahan) ? $model['tamu']->desa_kelurahan : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['kecamatan'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->kecamatan) ? $model['tamu']->kecamatan : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['kota_kabupaten'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->kota_kabupaten) ? $model['tamu']->kota_kabupaten : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['provinsi'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->provinsi) ? $model['tamu']->provinsi : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['pekerjaan'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->pekerjaan) ? $model['tamu']->pekerjaan : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['instansi'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->instansi) ? $model['tamu']->instansi : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['keperluan'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->keperluan) ? $model['tamu']->keperluan : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['no_hp'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->no_hp) ? $model['tamu']->no_hp : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">PIC P0</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->acceptedByP0->user->name) ? $model['tamu']->acceptedByP0->user->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['comment_by_p0'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->comment_by_p0) ? $model['tamu']->comment_by_p0 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tamu']->attributeLabels()['accepted_time_p0'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tamu']->accepted_time_p0) ? $model['tamu']->accepted_time_p0 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>

<?php if($model['tamu']->getTableType() != 1): ?>
<hr>
<?php endif; ?>

<?php if($model['tamu']->getTableType() == 1): ?>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">Diteruskan Kepada</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->p1ByP0->name) ? $model['tiket_eskalasi']->p1ByP0->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">PIC P1</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->acceptedByP1->user->name) ? $model['tiket_eskalasi']->acceptedByP1->user->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['comment_by_p1'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->comment_by_p1) ? $model['tiket_eskalasi']->comment_by_p1 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['accepted_time_p1'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->accepted_time_p1) ? $model['tiket_eskalasi']->accepted_time_p1 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">Diteruskan Kepada</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->p2ByP1->name) ? $model['tiket_eskalasi']->p2ByP1->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0">PIC P2</div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->acceptedByP2->user->name) ? $model['tiket_eskalasi']->acceptedByP2->user->name : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['comment_by_p2'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->comment_by_p2) ? $model['tiket_eskalasi']->comment_by_p2 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['accepted_time_p2'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->accepted_time_p2) ? $model['tiket_eskalasi']->accepted_time_p2 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['due_date_by_p2'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->due_date_by_p2) ? $model['tiket_eskalasi']->due_date_by_p2 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>

<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['finish_comment_p2'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->finish_comment_p2) ? $model['tiket_eskalasi']->finish_comment_p2 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_eskalasi']->attributeLabels()['finish_date_p2'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_eskalasi']->finish_date_p2) ? $model['tiket_eskalasi']->finish_date_p2 : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>

<?php elseif($model['tamu']->getTableType() == 2): ?>
<div class="box margin-bottom-10">
    <div class="box-2 text-right padding-x-0"><?= $model['tiket_mediasi']->attributeLabels()['pic'] ?></div>
    <div class="box-10 text-dark padding-x-0 padding-left-20 m-padding-left-0"><?= isset($model['tiket_mediasi']->pic) ? $model['tiket_mediasi']->pic : '<span class="text-gray f-italic">kosong</span>' ?></div>
</div>
<hr>
<?php endif; ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>