<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/tamu/list-p0-riwayat-mediasi.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <!-- <th>Action</th> -->
            <th class="text-dark f-normal" style="border-bottom: 1px">No Request</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">NIK</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Nama Pemohon</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Keperluan</th>
        </tr>
        <tr class="dt-search">
            <!-- <th class="padding-0"></th> -->
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search no request" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nik" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search nama pemohon" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search keperluan" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>