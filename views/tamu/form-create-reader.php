<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

$this->title = 'Tambah Pemohon menggunakan <i>Reader / Scanner</i>';
$this->params['breadcrumbs'][] = $this->title;

technosmart\assets_manager\Select2Asset::register($this);
technosmart\assets_manager\BootstrapDatepickerAsset::register($this);
technosmart\assets_manager\JqueryMaskedInputAsset::register($this);
technosmart\assets_manager\JqueryInputLimiterAsset::register($this);
technosmart\assets_manager\AutosizeAsset::register($this);
technosmart\assets_manager\VueDefaultValueAsset::register($this);
technosmart\assets_manager\VueResourceAsset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-70 m-margin-left-0">
    <div class="col-xs-6">    
<?php endif; ?>
    
    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app', 'class' => 'margin-bottom-40 margin-top-20']]); ?>

        <input class='hidden' type='text' name='TipeTiket' value="<?=$model['tipe_tiket']?>" v-model='tipe_tiket' v-default-value="<?=$model['tipe_tiket']?>">

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['tamu'], 'no_request')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'no_request', ['class' => 'control-label']); ?>
                    <div class="box">
                        <div class="box-10 margin-top-0 padding-x-0">
                            <?= Html::activeTextInput($model['tamu'], 'no_request', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.no_request', 'v-default-value' => '"' . $model['tamu']->no_request . '"']); ?>
                        </div>
                        <div class="box-2 margin-top-0 padding-x-0">
                            <?= Html::button('<i class="fa fa-download"></i>', ['class' => 'btn btn-block bg-azure border-azure hover-bg-light-blue', 'style' => 'height: 34px', 'v-on:click' => 'loadReader()']) ?>
                        </div>
                    </div>
                    <?= Html::error($model['tamu'], 'no_request', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'no_request')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['tamu'], 'nik')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'nik', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'nik', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.nik', 'v-default-value' => '"' . $model['tamu']->nik . '"']); ?>
                    <?= Html::error($model['tamu'], 'nik', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'nik')->end(); ?>
            </div>
        </div>
        
        <hr>

        <?= $form->field($model['tamu'], 'nama')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'nama', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['tamu'], 'nama', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.nama', 'v-default-value' => '"' . $model['tamu']->nama . '"']); ?>
            <?= Html::error($model['tamu'], 'nama', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'nama')->end(); ?>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['tamu'], 'tempat_lahir')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'tempat_lahir', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'tempat_lahir', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.tempat_lahir', 'v-default-value' => '"' . $model['tamu']->tempat_lahir . '"']); ?>
                    <?= Html::error($model['tamu'], 'tempat_lahir', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'tempat_lahir')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['tamu'], 'tanggal_lahir')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'tanggal_lahir', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'tanggal_lahir', ['class' => 'form-control input-mask-datt input-datepicket', 'maxlength' => true, 'v-model' => 'tamu.tanggal_lahir', 'v-default-value' => '"' . $model['tamu']->tanggal_lahir . '"']); ?>
                    <div class="fs-11"><?= $model['tamu']->attributeHints()['tanggal_lahir'] ? $model['tamu']->attributeHints()['tanggal_lahir'] : null ?></div>
                    <?= Html::error($model['tamu'], 'tanggal_lahir', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'tanggal_lahir')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['tamu'], 'jenis_kelamin')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'jenis_kelamin', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'jenis_kelamin', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.jenis_kelamin', 'v-default-value' => '"' . $model['tamu']->jenis_kelamin . '"']); ?>
                    <div class="fs-11"><?= $model['tamu']->attributeHints()['jenis_kelamin'] ? $model['tamu']->attributeHints()['jenis_kelamin'] : null ?></div>
                    <?= Html::error($model['tamu'], 'jenis_kelamin', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'jenis_kelamin')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['tamu'], 'agama')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'agama', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'agama', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.agama', 'v-default-value' => '"' . $model['tamu']->agama . '"']); ?>
                    <?= Html::error($model['tamu'], 'agama', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'agama')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['tamu'], 'no_kartu_keluarga')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'no_kartu_keluarga', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'no_kartu_keluarga', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.no_kartu_keluarga', 'v-default-value' => '"' . $model['tamu']->no_kartu_keluarga . '"']); ?>
                    <?= Html::error($model['tamu'], 'no_kartu_keluarga', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'no_kartu_keluarga')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['tamu'], 'golongan_darah')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'golongan_darah', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'golongan_darah', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.golongan_darah', 'v-default-value' => '"' . $model['tamu']->golongan_darah . '"']); ?>
                    <?= Html::error($model['tamu'], 'golongan_darah', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'golongan_darah')->end(); ?>
            </div>
        </div>

        <?= $form->field($model['tamu'], 'alamat')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'alamat', ['class' => 'control-label']); ?>
            <?= Html::activeTextArea($model['tamu'], 'alamat', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true, 'v-model' => 'tamu.alamat', 'v-default-value' => '"' . $model['tamu']->alamat . '"']); ?>
            <?= Html::error($model['tamu'], 'alamat', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'alamat')->end(); ?>

        <div class="box box-break-sm">
            <div class="box-2 padding-x-0">
                <?= $form->field($model['tamu'], 'rt')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'rt', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'rt', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.rt', 'v-default-value' => '"' . $model['tamu']->rt . '"']); ?>
                    <?= Html::error($model['tamu'], 'rt', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'rt')->end(); ?>
            </div>
            <div class="box-2 padding-right-0">
                <?= $form->field($model['tamu'], 'rw')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'rw', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'rw', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.rw', 'v-default-value' => '"' . $model['tamu']->rw . '"']); ?>
                    <?= Html::error($model['tamu'], 'rw', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'rw')->end(); ?>
            </div>
            <div class="box-4 padding-right-0">
                <?= $form->field($model['tamu'], 'desa_kelurahan')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'desa_kelurahan', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'desa_kelurahan', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.desa_kelurahan', 'v-default-value' => '"' . $model['tamu']->desa_kelurahan . '"']); ?>
                    <?= Html::error($model['tamu'], 'desa_kelurahan', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'desa_kelurahan')->end(); ?>
            </div>
            <div class="box-4 padding-right-0">
                <?= $form->field($model['tamu'], 'kecamatan')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'kecamatan', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'kecamatan', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.kecamatan', 'v-default-value' => '"' . $model['tamu']->kecamatan . '"']); ?>
                    <?= Html::error($model['tamu'], 'kecamatan', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'kecamatan')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['tamu'], 'kota_kabupaten')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'kota_kabupaten', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'kota_kabupaten', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.kota_kabupaten', 'v-default-value' => '"' . $model['tamu']->kota_kabupaten . '"']); ?>
                    <?= Html::error($model['tamu'], 'kota_kabupaten', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'kota_kabupaten')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['tamu'], 'provinsi')->begin(); ?>
                    <?= Html::activeLabel($model['tamu'], 'provinsi', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['tamu'], 'provinsi', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.provinsi', 'v-default-value' => '"' . $model['tamu']->provinsi . '"']); ?>
                    <?= Html::error($model['tamu'], 'provinsi', ['class' => 'help-block']); ?>
                <?= $form->field($model['tamu'], 'provinsi')->end(); ?>
            </div>
        </div>

        <?= $form->field($model['tamu'], 'pekerjaan')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'pekerjaan', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['tamu'], 'pekerjaan', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.pekerjaan', 'v-default-value' => '"' . $model['tamu']->pekerjaan . '"']); ?>
            <?= Html::error($model['tamu'], 'pekerjaan', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'pekerjaan')->end(); ?>
        <hr>

        <?= $form->field($model['tamu'], 'instansi')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'instansi', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['tamu'], 'instansi', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.instansi', 'v-default-value' => '"' . $model['tamu']->instansi . '"']); ?>
            <?= Html::error($model['tamu'], 'instansi', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'instansi')->end(); ?>

        <?= $form->field($model['tamu'], 'keperluan')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'keperluan', ['class' => 'control-label']); ?>
            <?= Html::activeTextArea($model['tamu'], 'keperluan', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true, 'v-model' => 'tamu.keperluan', 'v-default-value' => '"' . $model['tamu']->keperluan . '"']); ?>
            <?= Html::error($model['tamu'], 'keperluan', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'keperluan')->end(); ?>

        <?= $form->field($model['tamu'], 'no_hp')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'no_hp', ['class' => 'control-label']); ?>
            <?= Html::activeTextInput($model['tamu'], 'no_hp', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tamu.no_hp', 'v-default-value' => '"' . $model['tamu']->no_hp . '"']); ?>
            <div class="fs-11"><?= $model['tamu']->attributeHints()['no_hp'] ? $model['tamu']->attributeHints()['no_hp'] : null ?></div>
            <?= Html::error($model['tamu'], 'no_hp', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'no_hp')->end(); ?>
        <hr>

        <?= $form->field($model['tamu'], 'comment_by_p0')->begin(); ?>
            <?= Html::activeLabel($model['tamu'], 'comment_by_p0', ['class' => 'control-label']); ?>
            <?= Html::activeTextArea($model['tamu'], 'comment_by_p0', ['class' => 'form-control limited textarea-autosize', 'maxlength' => true, 'v-model' => 'tamu.comment_by_p0', 'v-default-value' => '"' . $model['tamu']->comment_by_p0 . '"']); ?>
            <?= Html::error($model['tamu'], 'comment_by_p0', ['class' => 'help-block']); ?>
        <?= $form->field($model['tamu'], 'comment_by_p0')->end(); ?>
        <hr>

        <div class="form-group radio-elegant">
            <label class="control-label">Jenis Permohonan</label>
            <div class="row row-radio">
                <div class="radio col-xs-3">
                    <label>
                        <input name="radio-column" value="1" type="radio" v-on:click="setTipeTiket(1)">
                        <i></i>Eskalasi
                    </label>
                </div>
                <div class="radio col-xs-3">
                    <label>
                        <input name="radio-column" value="2" type="radio" v-on:click="setTipeTiket(2)">
                        <i></i>Mediasi
                    </label>
                </div>
                <div class="radio col-xs-4">
                    <label>
                        <input name="radio-column" value="3" type="radio" v-on:click="setTipeTiket(3)">
                        <i></i>Selesai di Tempat
                    </label>
                </div>
            </div>
        </div>

        <div v-if="(tipe_tiket==1)" v-init>
            <?= $form->field($model['tiket_eskalasi'], 'p1_by_p0')->begin(); ?>
                <?= Html::activeLabel($model['tiket_eskalasi'], 'p1_by_p0', ['class' => 'control-label', 'label' => 'Diteruskan Kepada']); ?>
                <?= Html::activeDropDownList(
                    $model['tiket_eskalasi'],
                    'p1_by_p0',
                    ArrayHelper::map(
                        // (new \yii\db\Query())->select("id, name")->from("department")->where("parent IS NULL")->orderby("name")->all(),
                        \Yii::$app->dba->createCommand(
                            "SELECT d.id, d.name FROM department d
                            WHERE
                                d.parent IS NULL AND
                                d.id = :id", ['id' => Yii::$app->user->identity->user->id_department]
                        )->queryAll(),
                        'id',
                        'name'
                    ),
                    [
                        'class' => 'form-control select2',
                        'prompt' => '-- Pilih Data --',
                        'maxlength' => true,
                        'v-model' => 'tiket_eskalasi.p1_by_p0', 'v-default-value' => '"' . $model['tiket_eskalasi']->p1_by_p0 . '"'
                    ]
                ); ?>
                <?= Html::error($model['tiket_eskalasi'], 'p1_by_p0', ['class' => 'help-block']); ?>
            <?= $form->field($model['tiket_eskalasi'], 'p1_by_p0')->end(); ?>
        </div>

        <div v-if="(tipe_tiket==2)" v-init>
            <?= $form->field($model['tiket_mediasi'], 'pic')->begin(); ?>
                <?= Html::activeLabel($model['tiket_mediasi'], 'pic', ['class' => 'control-label']); ?>
                <?= Html::activeDropDownList(
                    $model['tiket_mediasi'],
                    'pic',
                    ArrayHelper::map(
                        \Yii::$app->db->createCommand(
                            "SELECT u.username, CONCAT(u.name, ' - ', COALESCE(d.alias, ''), ' (no Hp: ' , COALESCE(ua.no_hp, 'tidak ada') , ')') as name FROM `user` u
                            JOIN auth_assignment aa
                            ON u.id = aa.user_id
                            JOIN ptsa.user ua
                            ON u.id = ua.id
                            JOIN ptsa.department d
                            ON ua.id_department = d.id
                            WHERE
                                aa.item_name = 'mediator'"
                        )->queryAll(),
                        'username',
                        'name'
                    ),
                    [
                        'class' => 'form-control select2',
                        'prompt' => '-- Pilih Data --',
                        'maxlength' => true,
                        'v-model' => 'tiket_mediasi.pic', 'v-default-value' => '"' . $model['tiket_mediasi']->pic . '"'
                    ]
                ); ?>
                <?= Html::error($model['tiket_mediasi'], 'pic', ['class' => 'help-block']); ?>
            <?= $form->field($model['tiket_mediasi'], 'pic')->end(); ?>

            <?= $form->field($model['tiket_mediasi'], 'no_hp')->begin(); ?>
                <?= Html::activeLabel($model['tiket_mediasi'], 'no_hp', ['class' => 'control-label']); ?>
                <?= Html::activeTextInput($model['tiket_mediasi'], 'no_hp', ['class' => 'form-control', 'maxlength' => true, 'v-model' => 'tiket_mediasi.no_hp', 'v-default-value' => '"' . $model['tiket_mediasi']->no_hp . '"']); ?>
                <div class="fs-11">Harap isi kolom ini <span class="text-dark">jika no HP PIC Mediasi tidak ada / perlu di revisi</span></div>
                <div class="fs-11">Kosongkan kolom ini <span class="text-dark">jika no HP PIC Mediasi sudah benar</span></div>
                <?= Html::error($model['tiket_mediasi'], 'no_hp', ['class' => 'help-block']); ?>
            <?= $form->field($model['tiket_mediasi'], 'no_hp')->end(); ?>

        </div>
        <hr v-if="tipe_tiket==1 || tipe_tiket==2">

        <?= Html::submitButton('<i class="fa fa-save margin-right-10"></i>Submit', ['class' => 'btn btn-fancy bg-azure border-azure hover-bg-light-blue', 'v-if' => 'tipe_tiket!=0']) ?>
    
    <?php ActiveForm::end(); ?>
        
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>