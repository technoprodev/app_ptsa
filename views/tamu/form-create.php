<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model common\models\Tamu */
/* @var $form common\yii\widgets\ActiveForm */
$this->title = 'Tambah Pemohon';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="<?= \Yii::$app->controller->id ?>-<?= \Yii::$app->controller->action->id ?>" ng-controller="tamuController">
    <?php $form = ActiveForm::begin(['enableClientValidation' => true]); ?>
    
    <div class='row'>
        <div class='<?= \Yii::$app->request->isAjax ? "col-xs-12" : "col-xs-8" ?>'>
            <!-- [FORM SECTION TITLE] -->
            <div class='widget-box'>
                <div class="widget-header">
                    <h5 class="widget-title"><?= \Yii::$app->controller->action->id ?> <?= \Yii::$app->controller->id ?></h5> <div class="widget-toolbar"><a href="#" data-action="collapse"> <i class="ace-icon fa fa-chevron-up"></i> </a></div>
                </div>

                <div class="widget-body"><div class="widget-main clearfix">
                    <input class='hidden' type='text' name='TipeTiket' value="<?=$model['tipe_tiket']?>" initial-value ng-model='tipe_tiket'>

                    <?= $form->field($model['tamu'], 'no_request')->textInput(['maxlength' => true, 'readonly' => '', 'initial-value ng-model'=>'tamu.no_request']) ?>

                    <?= $form->field($model['tamu'], 'nik')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.nik']) ?>

                    <div class='form-group'>
                        <label class='control-label col-md-3 col-xs-12' for='load'></label>
                        <div class='col-md-8 col-xs-12'>
                            <?= Html::button('<span><i class="fa fa-download bigger-110 blue"> Load</i><span class="hidden">Load</span></span>', ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold', 'ng-click' => 'f.load()']) ?>
                        </div>
                    </div>
                    <hr>

                    <?= $form->field($model['tamu'], 'nama')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.nama']) ?>

                    <?= $form->field($model['tamu'], 'no_kartu_keluarga')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.no_kartu_keluarga']) ?>

                    <?= $form->field($model['tamu'], 'tempat_lahir')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.tempat_lahir']) ?>

                    <?= $form->field($model['tamu'], 'tanggal_lahir', ['options'=>['ng-if'=>'1==1'], "inputOptions" => [
                        'class' => 'form-control input-mask-date input-date-picker',
                    ]])->textInput(['maxlength' => true, 'initial-value ng-model'=>'$parent.tamu.tanggal_lahir']) ?>

                    <?= $form->field($model['tamu'], 'jenis_kelamin')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.jenis_kelamin']) ?>

                    <?= $form->field($model['tamu'], 'golongan_darah')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.golongan_darah']) ?>

                    <?= $form->field($model['tamu'], 'alamat')->textArea(['maxlength' => true, 'initial-value ng-model'=>'tamu.alamat']) ?>

                    <?= $form->field($model['tamu'], 'rt')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.rt']) ?>

                    <?= $form->field($model['tamu'], 'rw')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.rw']) ?>

                    <?= $form->field($model['tamu'], 'desa_kelurahan')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.desa_kelurahan']) ?>

                    <?= $form->field($model['tamu'], 'kecamatan')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.kecamatan']) ?>

                    <?= $form->field($model['tamu'], 'kota_kabupaten')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.kota_kabupaten']) ?>

                    <?= $form->field($model['tamu'], 'provinsi')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.provinsi']) ?>

                    <?= $form->field($model['tamu'], 'agama')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.agama']) ?>

                    <?= $form->field($model['tamu'], 'pekerjaan')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.pekerjaan']) ?>
                    <hr>

                    <?= $form->field($model['tamu'], 'keperluan')->textArea(['maxlength' => true, 'initial-value ng-model'=>'tamu.keperluan']) ?>

                    <?= $form->field($model['tamu'], 'no_hp')->textInput(['maxlength' => true, 'initial-value ng-model'=>'tamu.no_hp']) ?>
                    <hr>

                    <?= $form->field($model['tamu'], 'comment_by_p0')->textArea(['maxlength' => true, 'initial-value ng-model'=>'tamu.comment_by_p0']) ?>
                    <hr>

                    <?= Html::a('<span><i class="fa fa-edit bigger-110 grey"> Eskalasi</i><span class="hidden">Eskalasi</span></span>', null, ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-bold', 'ng-click' => 'f.setTipeTiket(1)']) ?>
                    <?= Html::a('<span><i class="fa fa-edit bigger-110 grey"> Mediasi </i><span class="hidden">Mediasi </span></span>', null, ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-bold', 'ng-click' => 'f.setTipeTiket(2)']) ?>
                    <?= Html::a('<span><i class="fa fa-edit bigger-110 grey"> Selesai di tempat</i><span class="hidden">Selesai </span></span>', null, ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-bold', 'ng-click' => 'f.setTipeTiket(3)']) ?>
                    <hr>

                    <?= $form->field($model['tiket_eskalasi'], 'p1_by_p0', ['options'=>['ng-if'=>'tipe_tiket==1']])
                        ->dropDownList(
                            ArrayHelper::map(
                                // (new \yii\db\Query())->select("id, name")->from("department")->where("parent IS NULL")->orderby("name")->all(),
                                \Yii::$app->db->createCommand(
                                    "SELECT d.id, d.name FROM department d
                                    WHERE
                                        d.parent IS NULL AND
                                        d.id = :id", ['id' => \Yii::$app->user->identity->user->id_department]
                                    )->queryAll(),
                                "id", "name"
                            ),
                            [
                                'prompt' => '-- Pilih Data --',
                                'maxlength' => true,
                                'initial-value' => $model['tiket_eskalasi']->p1_by_p0,
                                'ng-model' => 'tiket_eskalasi.p1_by_p0'
                            ]
                        ) ?>
                    
                    <?= $form->field($model['tiket_mediasi'], 'pic', ['options'=>['ng-if'=>'tipe_tiket==2']])
                        ->dropDownList(
                            ArrayHelper::map(
                                \Yii::$app->db->createCommand(
                                    "SELECT u.username, CONCAT(u.name::text , ' (no Hp: ' , COALESCE(u.no_hp::text, 'tidak ada') , ')') as name FROM \"user\" u
                                    JOIN auth_assignment aa
                                    ON u.id::varchar = aa.user_id
                                    WHERE
                                        aa.item_name = 'mediator'"
                                    )->queryAll(),
                                "username", "name"
                            ),
                            [
                                'prompt' => '-- Pilih Data --',
                                'maxlength' => true,
                                'initial-value' => $model['tiket_mediasi']->pic,
                                'ng-model'=>'tiket_mediasi.pic'
                            ]
                        ) ?>
                    <?= $form->field($model['tiket_mediasi'], 'no_hp', ['options'=>['ng-if'=>'tipe_tiket==2']])->textInput(['maxlength' => true, 'initial-value ng-model'=>'tiket_mediasi.no_hp', 'placeholder' => 'Jika no hp PIC mediasi tidak ada  atau salah, harap isi kolom ini'])->label('No Hp revisi')?>
                    <hr ng-if = 'tipe_tiket==1 || tipe_tiket==2' >
                    
                    <div class='clearfix'>
                        <?= Html::submitButton('<span><i class="fa fa-save bigger-110 blue"> Submit</i><span class="hidden">Submit</span></span>', ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold pull-right', 'ng-if' => 'tipe_tiket!=0']) ?>

                        <!-- Html::button('<span><i class="fa fa-save bigger-110 blue"> Submit</i><span class="hidden">Submit</span></span>', ['class' => 'dt-button buttons-collection buttons-colvis btn btn-white btn-primary btn-bold pull-right', 'ng-if' => 'tipe_tiket!=0', 'ng-click' => 'f.submit()']) -->
                    </div>
                    <hr ng-if = 'tipe_tiket!=0' >
                    
                </div></div>
            </div>
            
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <hr>
</div>