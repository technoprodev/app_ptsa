<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
$this->context->layout = 'error';
?>
<div class="margin-top-50 border-light-red bg-lightest rounded-xs" style="max-width: 400px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="alert alert-danger margin-0">
        <?= nl2br(Html::encode($message)) ?>
    </div>
    <div class="padding-15">
        <div class="margin-bottom-15"> The above error occurred while the Web server was processing your request. </div>
        <div class="margin-bottom-15"> Please contact us if you think this is a server error. Thank you. </div>
        <div><a href="<?= Yii::$app->getRequest()->getBaseUrl() ?>">Back to Home</a></div>
    </div>
</div>