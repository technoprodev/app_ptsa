<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="text-center margin-top-60 m-margin-top-30">
    <img src="<?= Yii::$app->getRequest()->getBaseUrl() ?>/img/logo-inverse.png" width="50px;" class="bg-dark-azure shadow circle padding-5">
</div>
<div class="margin-top-20 border-lighter border-thick shadow rounded-xs" style="max-width: 350px; width: 100%; margin-left: auto; margin-right: auto;">
    <div class="clearfix padding-15 border-bottom bg-lighter text-center fs-18">
        <span class="text-dark-azure f-italic">kemnaker</span><span class="f-normal margin-x-5 bg-azure padding-x-5 padding-y-5 rounded-sm">PTSA</span>
    </div>
    <div class="padding-20 bg-lightest">
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div class="form-group">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="bg-azure padding-y-15 text-lighter" style="background-color: #5090C1">
        <div class="text-center fs-21">Pelayanan Terpadu Satu Atap</div>
        <div class="text-center fs-12 margin-top-5 f-bold"><?= date('Y') ?> &copy; KEMENTERIAN KETENAGAKERJAAN R.I.</div>
    </div>
</div>