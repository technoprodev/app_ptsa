<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'Home';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-break-sm box-space-sm equal">
	<div class="box-4 border-light shadow padding-20">
		<h5 class="text-azure">PTSA Kemnaker</h5>
		<div>PTSA (Pelayanan Terpadu Satu Atap) Merupakan perpaduan beberapa jenis pelayanan dari masing-masing satuan kerja penyelenggara, mulai dari tahap permohonan sampai dengan tahap penyelesaian produk pelayanan.</div>
	</div>
	<div class="box-3 border-light shadow padding-20">
		<h5 class="text-azure">Jenis Pelayanan</h5>
		<div class="box box-break-sm">
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Perizinan</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Pendaftaran</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Pengesahan</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Pencatatan</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Rekomendasi</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Persetujuan</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Penunjukan</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Konsultasi</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Lainnya</div>
		</div>
	</div>
	<div class="box-4 border-light shadow padding-20">
		<h5 class="text-azure">Satuan Kerja / Loket</h5>
		<div class="box box-break-sm">
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket Setjen/ PPID</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket PHI & Jamsos</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket Binwasnaker & K3</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket Binapenta & PKK</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket Itjen / WBS</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket Binalattas</div>
			<div class="box-6 padding-left-0"><i class="fa fa-check margin-right-5"></i>Loket Barenbang</div>
		</div>
	</div>
</div>

<div class="box box-break-sm box-space-sm equal">
	<div class="box-4 border-light shadow padding-20">
		<h5 class="text-azure">Jadwal Pelayanan</h5>
		<div class="text-center fs-16 f-bold">
			<div class="margin-top-40">Pukul 08.00 s.d 15.00 – Pendaftaran Antrian</div>
			<div class="margin-top-5">Pukul 08.00 s.d 16.00 – Pelayanan di Loket</div>
		</div>
	</div>
	<div class="box-7 border-light shadow padding-20">
		<h5 class="text-azure">Contact Person</h5>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>Setjen/PPID</b> : Syaefudin, S.I. Pem,M.Si/HP - <span class="text-azure">0811 9864 69</span></div>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>PHI dan Jamsos</b> : Kresensia Harianja/HP - <span class="text-azure">0812 8622 9796</span></div>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>Binwasnaker dan K3</b> : Cut Adee Opie R, ST/HP - <span class="text-azure">0813 6198 2981</span></div>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>Binapenta dan PKK</b> : Tredy Ramadhani, S.Sos/HP - <span class="text-azure">0859 5950 0757</span></div>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>Itjen/WBS</b> : Abdillah, SE, M.Si/HP - <span class="text-azure">0812 1268 3450</span></div>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>Binalattas</b> : Agus Zakarya, S.Pd/HP - <span class="text-azure">0812 1981 343</span></div>
		<div><i class="fa fa-angle-right margin-right-5"></i><b>Barenbang</b> : Rusita Danudilaga, S.Kom, M.Si/HP - <span class="text-azure">0812 1001 3051</span></div>
	</div>
</div>