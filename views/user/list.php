<?php

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/user/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-normal" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Name</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Username</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Email</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Status</th>
            <th class="text-dark f-normal" style="border-bottom: 1px">Department</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search name" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search username" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search email" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search status" class="form-control border-none f-normal padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search department" class="form-control border-none f-normal padding-x-5"/></th>
        </tr>
    </thead>
</table>