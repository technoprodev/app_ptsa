<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\Select2Asset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-70 m-margin-left-0">
    <div class="col-xs-6">    
<?php endif; ?>
    
    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['class' => 'margin-bottom-40 margin-top-20']]); ?>

        <div class="box box-break-sm">
            <div class="box-12 padding-x-0">
                <?= $form->field($model['user'], 'name')->begin(); ?>
                    <?= Html::activeLabel($model['user'], 'name', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['user'], 'name', ['class' => 'help-block']); ?>
                <?= $form->field($model['user'], 'name')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['user'], 'username')->begin(); ?>
                    <?= Html::activeLabel($model['user'], 'username', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['user'], 'username', ['class' => 'help-block']); ?>
                <?= $form->field($model['user'], 'username')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['user'], 'email')->begin(); ?>
                    <?= Html::activeLabel($model['user'], 'email', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['user'], 'email', ['class' => 'help-block']); ?>
                <?= $form->field($model['user'], 'email')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['userApp'], 'no_hp')->begin(); ?>
                    <?= Html::activeLabel($model['userApp'], 'no_hp', ['class' => 'control-label']); ?>
                    <?= Html::activeTextInput($model['userApp'], 'no_hp', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['userApp'], 'no_hp', ['class' => 'help-block']); ?>
                <?= $form->field($model['userApp'], 'no_hp')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['userApp'], 'id_department')->begin(); ?>
                    <?= Html::activeLabel($model['userApp'], 'id_department', ['class' => 'control-label']); ?>
                    <?= Html::activeDropDownList(
                        $model['userApp'],
                        'id_department',
                        ArrayHelper::map(
                            \Yii::$app->dba->createCommand(
                                "SELECT d.id, d.name FROM department d", []
                                )->queryAll(),
                            'id', 'name'
                        ),
                        [
                            'class' => 'form-control select2',
                            'prompt' => '-- Pilih Data --',
                            'maxlength' => true,
                        ]
                    ); ?>
                    <?= Html::error($model['userApp'], 'id_department', ['class' => 'help-block']); ?>
                <?= $form->field($model['userApp'], 'id_department')->end(); ?>
            </div>
        </div>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['user'], 'status', ['options' => ['class' => 'form-group radio-elegant']])->begin(); ?>
                    <?= Html::activeLabel($model['user'], 'status', ['class' => 'control-label']); ?>
                    <?= Html::activeRadioList($model['user'], 'status', ['1' => 'Active', '0' => 'Inactive', '-1' => 'Deleted'], ['unselect' => null,
                        'item' => function($index, $label, $name, $checked, $value){
                            $disabled = in_array($value, []) ? true : false;

                            $radio = Html::radio($name, $checked, ['value' => $value, 'disabled' => $disabled]);
                            return Html::tag('div', Html::label($radio . '<i></i>' . $label), ['class' => 'radio']);

                            // equal to this
                            // $disabled = in_array($value, ['val1', 'val2', '1']) ? 'disabled' : '';
                            // return "<div class='radio'><label><input type='radio' name='$name' value='$value' $disabled><i></i>My Label</label></div>";
                        }]); ?>
                    <?= Html::error($model['user'], 'status', ['class' => 'help-block']); ?>
                <?= $form->field($model['user'], 'status')->end(); ?>
            </div>
        </div>

        <?php if($model['user']->isNewRecord) { ?>
        <!-- <= $form->field($model['user'], 'password')->passwordInput(['maxlength' => 255]) ?> -->

        <!-- <= $form->field($model['user'], 'repassword')->passwordInput(['maxlength' => 255]) ?> -->
        <?php } ?>

        <div class="box box-break-sm">
            <div class="box-12 padding-x-0">
                <div class="form-group field-roles-assignments">
                    <label class="control-label">Roles</label>
                    <?= Html::checkboxList(
                        'assignments[]',
                        $model['assignments'],
                        $roles
                    ) ?>
                </div>
            </div>
        </div>

        <?= Html::submitButton('<i class="fa fa-save margin-right-10"></i>Submit', ['class' => 'btn btn-fancy bg-azure border-azure hover-bg-light-blue']) ?>
    
    <?php ActiveForm::end(); ?>
        
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>