<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\Select2Asset::register($this);
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="row margin-left-70 m-margin-left-0">
    <div class="col-xs-6">    
<?php endif; ?>
    
    <?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['class' => 'margin-bottom-40 margin-top-20']]); ?>

        <div class="box box-break-sm">
            <div class="box-6 padding-x-0">
                <?= $form->field($model['user'], 'password')->begin(); ?>
                    <?= Html::activeLabel($model['user'], 'password', ['class' => 'control-label', 'label' => 'New Password']); ?>
                    <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['user'], 'password', ['class' => 'help-block']); ?>
                <?= $form->field($model['user'], 'password')->end(); ?>
            </div>
            <div class="box-6 padding-right-0">
                <?= $form->field($model['user'], 'repassword')->begin(); ?>
                    <?= Html::activeLabel($model['user'], 'repassword', ['class' => 'control-label']); ?>
                    <?= Html::activePasswordInput($model['user'], 'repassword', ['class' => 'form-control', 'maxlength' => true]); ?>
                    <?= Html::error($model['user'], 'repassword', ['class' => 'help-block']); ?>
                <?= $form->field($model['user'], 'repassword')->end(); ?>
            </div>
        </div>

        <?= Html::submitButton('<i class="fa fa-save margin-right-10"></i>Submit', ['class' => 'btn btn-fancy bg-azure border-azure hover-bg-light-blue']) ?>
    
    <?php ActiveForm::end(); ?>
        
<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>