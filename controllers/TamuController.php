<?php
namespace app_ptsa\controllers;

use Yii;
use app_ptsa\models\Tamu;
use app_ptsa\models\TamuCard;
use app_ptsa\models\TiketEskalasi;
use app_ptsa\models\TiketMediasi;
use app_ptsa\models\User;
use app_ptsa\models\Department;
use app_ptsa\models\TamuSearch;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class TamuController extends Controller
{
    public static $permissions = [
        ['view', 'Detail Tiket'], ['p0', 'Hak Akses P0'], ['p1', 'Hak Akses P1'], ['p2', 'Hak Akses P2']
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['view'], 'view'],
                [[
                    'create',
                    'create-manual',
                    'create-service',
                    'create-reader',
                    'load',
                    'load-vpn',
                    'load-reader',
                    'reassignment-p0',
                    'p0-tracking-belum-p1',
                    'p0-tracking-belum-p2',
                    'p0-tracking-sedang-p2',
                    'p0-riwayat-eskalasi',
                    'p0-riwayat-mediasi',
                    'p0-riwayat-selesai',
                ], 'p0'],
                [[
                    'assignment-p1',
                    'reassignment-p1',
                    'p1-tracking-belum-p2',
                    'p1-tracking-sedang-p2',
                    'p1-riwayat-eskalasi',
                ], 'p1'],
                [[
                    'assignment-belum-p2',
                    'assignment-sedang-p2',
                    'p2-riwayat-eskalasi',
                ], 'p2'],
            ]),
        ];
    }

    // All
    public function actionView($id)
    {
        $model = [
            'tamu' => $this->findModel($id),
            'tiket_eskalasi' => TiketEskalasi::findOne($id),
            'tiket_mediasi' => TiketMediasi::findOne($id),
            'tipe_tiket' => 0,
        ];
        switch ($model['tamu']->getTableType()) {
            case 3:
                $model['tiket_eskalasi'] = new TiketEskalasi();
                $model['tiket_mediasi'] = new TiketMediasi();
                $model['tipe_tiket'] = 3;
                break;
            case 2:
                $model['tiket_eskalasi'] = new TiketEskalasi();
                $model['tipe_tiket'] = 2;
                break;
            case 1:
                $model['tiket_mediasi'] = new TiketMediasi();
                $model['tipe_tiket'] = 1;
                if (!isset($model['tiket_eskalasi']->acceptedByP1))
                    $model['tiket_eskalasi']->acceptedByP1 = new User();
                if (!isset($model['tiket_eskalasi']->acceptedByP2))
                    $model['tiket_eskalasi']->acceptedByP2 = new User();
                if (!isset($model['tiket_eskalasi']->p1ByP0))
                    $model['tiket_eskalasi']->p1ByP0 = new Department();
                if (!isset($model['tiket_eskalasi']->p2ByP1))
                    $model['tiket_eskalasi']->p2ByP1 = new Department();
                break;
        }

        return $this->render('one', [
            'title' => 'Informasi Tiket ' . $model['tamu']->no_request,
            'model' => $model
        ]);
    }

    // P0
    public function actionCreate()
    {
        $model = [
            'tamu' => new Tamu(),
            'tiket_eskalasi' => new TiketEskalasi(),
            'tiket_mediasi' => new TiketMediasi(),
            'tipe_tiket' => 0,
        ];

        $post = Yii::$app->request->post();

        if ($model['tamu']->load($post)) {
            /*\Yii::$app->db->transaction(function($db) {

            });*/
            $model['tamu']->accepted_by_p0 = \Yii::$app->user->identity->user->username;
            $model['tamu']->accepted_time_p0 = new \yii\db\Expression("now()");
            
            if ($model['tamu']->save()) {
                $model['tipe_tiket'] = $post['TipeTiket'];
                switch ($model['tipe_tiket']) {
                    case 1:
                        $model['tiket_eskalasi']->load($post);
                        $model['tiket_eskalasi']->no_request = $model['tamu']->no_request;
                        $model['tiket_eskalasi']->save();
                        break;
                    case 2:
                        $model['tiket_mediasi']->load($post);
                        $model['tiket_mediasi']->no_request = $model['tamu']->no_request;
                        if (!$model['tiket_mediasi']->pic) $model['tiket_mediasi']->pic = null;
                        if (!$model['tiket_mediasi']->save()) {
                            return $this->render('form-create', [
                                'model' => $model,
                            ]);
                        }
                        if (\Yii::$app->params['sms']) {
                            $message = 
                                'PTSA: Anda dibutuhkan di PTSA untuk proses mediasi, silahkan datang ke loket pelayanan PTSA dan menemui '.
                                $model['tamu']->accepted_by_p0 .
                                '. Terima kasih.';
                            if ($model['tiket_mediasi']->no_hp) {
                                $noHp = $model['tiket_mediasi']->no_hp;
                                $this->sms($message, $noHp);
                            }
                            else if ($model['tiket_mediasi']->pic0->no_hp) {
                                $noHp = $model['tiket_mediasi']->pic0->no_hp;
                                $this->sms($message, $noHp);
                            }
                        }
                        break;
                }

                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-create', [
                    'model' => $model,
                ]);
            }
        } else {
            $model['tamu']->no_request = $this->sequence('tamu-no_request');
            return $this->render('form-create', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateManual()
    {
        $model = [
            'tamu' => new Tamu(),
            'tiket_eskalasi' => new TiketEskalasi(),
            'tiket_mediasi' => new TiketMediasi(),
            'tipe_tiket' => 0,
        ];

        $post = Yii::$app->request->post();

        if ($model['tamu']->load($post)) {
            /*\Yii::$app->db->transaction(function($db) {

            });*/
            $model['tamu']->accepted_by_p0 = \Yii::$app->user->identity->user->username;
            $model['tamu']->accepted_time_p0 = new \yii\db\Expression("now()");
            
            if ($model['tamu']->save()) {
                $model['tipe_tiket'] = $post['TipeTiket'];
                switch ($model['tipe_tiket']) {
                    case 1:
                        $model['tiket_eskalasi']->load($post);
                        $model['tiket_eskalasi']->no_request = $model['tamu']->no_request;
                        $model['tiket_eskalasi']->save();
                        break;
                    case 2:
                        $model['tiket_mediasi']->load($post);
                        $model['tiket_mediasi']->no_request = $model['tamu']->no_request;
                        if (!$model['tiket_mediasi']->pic) $model['tiket_mediasi']->pic = null;
                        if (!$model['tiket_mediasi']->save()) {
                            return $this->render('form-create-manual', [
                                'model' => $model,
                            ]);
                        }
                        if (\Yii::$app->params['sms']) {
                            $message = 
                                'PTSA: Anda dibutuhkan di PTSA untuk proses mediasi, silahkan datang ke loket pelayanan PTSA dan menemui '.
                                $model['tamu']->accepted_by_p0 .
                                '. Terima kasih.';
                            if ($model['tiket_mediasi']->no_hp) {
                                $noHp = $model['tiket_mediasi']->no_hp;
                                $this->sms($message, $noHp);
                            }
                            else if ($model['tiket_mediasi']->pic0->no_hp) {
                                $noHp = $model['tiket_mediasi']->pic0->no_hp;
                                $this->sms($message, $noHp);
                            }
                        }
                        break;
                }

                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-create-manual', [
                    'model' => $model,
                ]);
            }
        } else {
            $model['tamu']->no_request = $this->sequence('tamu-no_request');
            return $this->render('form-create-manual', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateService()
    {
        $model = [
            'tamu' => new Tamu(),
            'tiket_eskalasi' => new TiketEskalasi(),
            'tiket_mediasi' => new TiketMediasi(),
            'tipe_tiket' => 0,
        ];

        $post = Yii::$app->request->post();

        if ($model['tamu']->load($post)) {
            /*\Yii::$app->db->transaction(function($db) {

            });*/
            $model['tamu']->accepted_by_p0 = \Yii::$app->user->identity->user->username;
            $model['tamu']->accepted_time_p0 = new \yii\db\Expression("now()");
            
            if ($model['tamu']->save()) {
                $model['tipe_tiket'] = $post['TipeTiket'];
                switch ($model['tipe_tiket']) {
                    case 1:
                        $model['tiket_eskalasi']->load($post);
                        $model['tiket_eskalasi']->no_request = $model['tamu']->no_request;
                        $model['tiket_eskalasi']->save();
                        break;
                    case 2:
                        $model['tiket_mediasi']->load($post);
                        $model['tiket_mediasi']->no_request = $model['tamu']->no_request;
                        if (!$model['tiket_mediasi']->pic) $model['tiket_mediasi']->pic = null;
                        if (!$model['tiket_mediasi']->save()) {
                            return $this->render('form-create-service', [
                                'model' => $model,
                            ]);
                        }
                        if (\Yii::$app->params['sms']) {
                            $message = 
                                'PTSA: Anda dibutuhkan di PTSA untuk proses mediasi, silahkan datang ke loket pelayanan PTSA dan menemui '.
                                $model['tamu']->accepted_by_p0 .
                                '. Terima kasih.';
                            if ($model['tiket_mediasi']->no_hp) {
                                $noHp = $model['tiket_mediasi']->no_hp;
                                $this->sms($message, $noHp);
                            }
                            else if ($model['tiket_mediasi']->pic0->no_hp) {
                                $noHp = $model['tiket_mediasi']->pic0->no_hp;
                                $this->sms($message, $noHp);
                            }
                        }
                        break;
                }

                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-create-service', [
                    'model' => $model,
                ]);
            }
        } else {
            $model['tamu']->no_request = $this->sequence('tamu-no_request');
            return $this->render('form-create-service', [
                'model' => $model,
            ]);
        }
    }

    public function actionCreateReader()
    {
        $model = [
            'tamu' => new Tamu(),
            'tiket_eskalasi' => new TiketEskalasi(),
            'tiket_mediasi' => new TiketMediasi(),
            'tipe_tiket' => 0,
        ];

        $post = Yii::$app->request->post();

        if ($model['tamu']->load($post)) {
            /*\Yii::$app->db->transaction(function($db) {

            });*/
            $model['tamu']->accepted_by_p0 = \Yii::$app->user->identity->user->username;
            $model['tamu']->accepted_time_p0 = new \yii\db\Expression("now()");
            
            if ($model['tamu']->save()) {
                $model['tipe_tiket'] = $post['TipeTiket'];
                switch ($model['tipe_tiket']) {
                    case 1:
                        $model['tiket_eskalasi']->load($post);
                        $model['tiket_eskalasi']->no_request = $model['tamu']->no_request;
                        $model['tiket_eskalasi']->save();
                        break;
                    case 2:
                        $model['tiket_mediasi']->load($post);
                        $model['tiket_mediasi']->no_request = $model['tamu']->no_request;
                        if (!$model['tiket_mediasi']->pic) $model['tiket_mediasi']->pic = null;
                        if (!$model['tiket_mediasi']->save()) {
                            return $this->render('form-create-reader', [
                                'model' => $model,
                            ]);
                        }
                        if (\Yii::$app->params['sms']) {
                            $message = 
                                'PTSA: Anda dibutuhkan di PTSA untuk proses mediasi, silahkan datang ke loket pelayanan PTSA dan menemui '.
                                $model['tamu']->accepted_by_p0 .
                                '. Terima kasih.';
                            if ($model['tiket_mediasi']->no_hp) {
                                $noHp = $model['tiket_mediasi']->no_hp;
                                $this->sms($message, $noHp);
                            }
                            else if ($model['tiket_mediasi']->pic0->no_hp) {
                                $noHp = $model['tiket_mediasi']->pic0->no_hp;
                                $this->sms($message, $noHp);
                            }
                        }
                        break;
                }

                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-create-reader', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('form-create-reader', [
                'model' => $model,
            ]);
        }
    }

    public function actionDatatablesReassignmentP0()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NULL')
                ->andWhere('te.accepted_by_p1 IS NULL')
                ->andWhere('te.reject_by_p1 IS NOT NULL')
                ->andWhere('te.reject_time_p1 IS NOT NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
                'reject_by_p1',
                'reject_time_p1',
                'reject_comment_by_p1',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionReassignmentP0($id=null)
    {
        // view all data
        if (!$id) {
            return $this->render('list-reassignment-p0', [
                'title' => 'Tiket Reassignment & <span class="text-dark">Belum Dikerjakan oleh P0</span>',
            ]);
        }

        $model = [
            'tamu' => $this->findModel($id),
            'tiket_eskalasi' => TiketEskalasi::findOne($id),
            'accept_tiket' => 0,
        ];

        $post = Yii::$app->request->post();
        if (!isset($post['Tamu'])) {
            $post['Tamu'] = [];
        }
        
        if ($model['tamu']->load($post) && $model['tiket_eskalasi']->load($post)) {
            $model['accept_tiket'] = $post['AcceptTiket'];
            switch ($model['accept_tiket']) {
                case 1:
                    $model['tiket_eskalasi']->scenario = 'eskalasi';
                    $model['tiket_eskalasi']->reassign_time_p0 = new \yii\db\Expression("now()");
                    $model['tiket_eskalasi']->reject_by_p1 = null;
                    $model['tiket_eskalasi']->reject_time_p1 = null;
                    $model['tiket_eskalasi']->reject_comment_by_p1 = null;
                    break;
                case 2:
                    $model['tiket_eskalasi']->reject_by_p1 = null;
                    $model['tiket_eskalasi']->reject_time_p1 = null;
                    $model['tiket_eskalasi']->reject_comment_by_p1 = null;
                    $model['tiket_eskalasi']->cancel_by_p0 = \Yii::$app->user->identity->user->username;
                    $model['tiket_eskalasi']->cancel_time_p0 = new \yii\db\Expression("now()");
                    break;
            }

            if ($model['tamu']->save() && $model['tiket_eskalasi']->save()) {
                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-reassignment-p0', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('form-reassignment-p0', [
                'model' => $model,
            ]);
        }
    }

    public function actionDatatablesP0TrackingBelumP1()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP0TrackingBelumP1()
    {
        return $this->render('list-p0-tracking-belum-p1', [
            'title' => 'Tracking Tiket yang <span class="text-grayest">Belum dikerjakan oleh P1</span>',
        ]);
    }

    public function actionDatatablesP0TrackingBelumP2()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP0TrackingBelumP2()
    {
        return $this->render('list-p0-tracking-belum-p2', [
            'title' => 'Tracking Tiket yang <span class="text-dark">Belum dikerjakan oleh P2</span>',
        ]);
    }

    public function actionDatatablesP0TrackingSedangP2()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NOT NULL')
                ->andWhere('te.due_date_by_p2 IS NOT NULL')
                ->andWhere('te.finish_date_p2 IS NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP0TrackingSedangP2()
    {
        return $this->render('list-p0-tracking-sedang-p2', [
            'title' => 'Tracking Tiket yang <span class="text-dark">Sedang dikerjakan oleh P2</span>',
        ]);
    }

    public function actionDatatablesP0RiwayatEskalasi()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NOT NULL')
                ->andWhere('te.due_date_by_p2 IS NOT NULL')
                ->andWhere('te.finish_date_p2 IS NOT NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP0RiwayatEskalasi()
    {
        return $this->render('list-p0-riwayat-eskalasi', [
            'title' => 'Riwayat Tiket yang <span class="text-dark">Selesai Melalui Eskalasi</span>',
        ]);
    }

    public function actionDatatablesP0RiwayatMediasi()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_mediasi tm', 't.no_request = tm.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP0RiwayatMediasi()
    {
        return $this->render('list-p0-riwayat-mediasi', [
            'title' => 'Riwayat Tiket yang <span class="text-dark">Selesai Melalui Mediasi</span>',
        ]);
    }

    public function actionDatatablesP0RiwayatSelesai()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('LEFT JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->join('LEFT JOIN', 'tiket_mediasi tm', 't.no_request = tm.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.no_request IS NULL')
                ->andWhere('tm.no_request IS NULL')
                ->andWhere('t.accepted_by_p0 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP0RiwayatSelesai()
    {
        return $this->render('list-p0-riwayat-selesai', [
            'title' => 'Riwayat Tiket yang <span class="text-dark">Selesai di Tempat</span>',
        ]);
    }

    // P1
    public function actionDatatablesAssignmentP1()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('LEFT JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NULL')
                ->andWhere('te.p2_by_p1 IS NULL')
                ->andWhere('te.accepted_by_p2 IS NULL')
                ->andWhere('te.reject_by_p2 IS NULL')
                ->andWhere('te.p1_by_p0 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionAssignmentP1($id=null)
    {
        // view all data
        if (!$id) {
            return $this->render('list-assignment-p1', [
                'title' => 'Tiket Assignment & <span class="text-dark">Belum Dikerjakan oleh P1</span>',
            ]);
        }

        $model = [
            'tamu' => $this->findModel($id),
            'tiket_eskalasi' => TiketEskalasi::findOne($id),
            'accept_tiket' => 0,
        ];

        if ($model['tamu']->getTableType() != 1)
            throw new \yii\web\HttpException('403', 'Tiket dengan request '.$model['tamu']->no_request.' bukan merupakan tiket Eskalasi');
        else if ($model['tiket_eskalasi']->getStateStatus()[0] != 2)
            throw new \yii\web\HttpException('403', $model['tiket_eskalasi']->getStateStatus()[1]);

        $post = Yii::$app->request->post();

        if ($model['tiket_eskalasi']->load($post)) {
            $model['accept_tiket'] = $post['AcceptTiket'];
            switch ($model['accept_tiket']) {
                case 1:
                    $model['tiket_eskalasi']->accepted_by_p1 = \Yii::$app->user->identity->user->username;
                    $model['tiket_eskalasi']->accepted_time_p1 = new \yii\db\Expression("now()");
                    if (\Yii::$app->params['sms']) {
                        $message = 
                            'KEMNAKER : Permohonan pelayanan dengan Nomor TIKET ' .
                            $model['tiket_eskalasi']->no_request .
                            ' sedang kami proses. Terima kasih atas kunjungan Anda.';
                        $this->sms($message, $model['tamu']->no_hp);
                    }
                    break;
                case 2:
                    $model['tiket_eskalasi']->p1_by_p0 = null;
                    $model['tiket_eskalasi']->accepted_by_p1 = null;
                    $model['tiket_eskalasi']->accepted_time_p1 = null;
                    $model['tiket_eskalasi']->comment_by_p1 = null;
                    $model['tiket_eskalasi']->reject_by_p1 = \Yii::$app->user->identity->user->username;
                    $model['tiket_eskalasi']->reject_time_p1 = new \yii\db\Expression("now()");
                    break;
            }

            if ($model['tiket_eskalasi']->save()) {
                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-assignment-p1', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('form-assignment-p1', [
                'model' => $model,
            ]);
        }
    }

    public function actionDatatablesReassignmentP1()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('LEFT JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NULL')
                ->andWhere('te.accepted_by_p2 IS NULL')
                ->andWhere('te.reject_by_p2 IS NOT NULL')
                ->andWhere('te.reject_time_p2 IS NOT NULL')
                ->andWhere('te.p1_by_p0 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
                'reject_by_p2',
                'reject_time_p2',
                'reject_comment_by_p2',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionReassignmentP1($id=null)
    {
        // view all data
        if (!$id) {
            return $this->render('list-reassignment-p1', [
                'title' => 'Tiket Reassignment & <span class="text-dark">Belum Dikerjakan oleh P1</span>',
            ]);
        }

        $model = [
            'tamu' => $this->findModel($id),
            'tiket_eskalasi' => TiketEskalasi::findOne($id),
            'accept_tiket' => 0,
        ];

        $post = Yii::$app->request->post();

        if ($model['tiket_eskalasi']->load($post)) {
            $model['tiket_eskalasi']->reject_by_p2 = null;
            $model['tiket_eskalasi']->reject_time_p2 = null;
            $model['tiket_eskalasi']->reject_comment_by_p2 = null;
            $model['accept_tiket'] = $post['AcceptTiket'];
            switch ($model['accept_tiket']) {
                case 1:
                    $model['tiket_eskalasi']->reassign_time_p1 = new \yii\db\Expression("now()");
                    break;
                case 2:
                    $model['tiket_eskalasi']->p1_by_p0 = null;
                    $model['tiket_eskalasi']->accepted_by_p1 = null;
                    $model['tiket_eskalasi']->accepted_time_p1 = null;
                    $model['tiket_eskalasi']->comment_by_p1 = null;
                    $model['tiket_eskalasi']->reject_by_p1 = \Yii::$app->user->identity->user->username;
                    $model['tiket_eskalasi']->reject_time_p1 = new \yii\db\Expression("now()");
                    break;
            }

            if ($model['tiket_eskalasi']->save()) {
                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-reassignment-p1', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('form-reassignment-p1', [
                'model' => $model,
            ]);
        }
    }

    public function actionDatatablesP1TrackingBelumP2()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NULL')
                ->andWhere('te.p1_by_p0 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP1TrackingBelumP2()
    {
        return $this->render('list-p1-tracking-belum-p2', [
            'title' => 'Tracking Tiket yang <span class="text-grayest">Belum dikerjakan oleh P2</span>',
        ]);
    }

    public function actionDatatablesP1TrackingSedangP2()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NOT NULL')
                ->andWhere('te.due_date_by_p2 IS NOT NULL')
                ->andWhere('te.finish_date_p2 IS NULL')
                ->andWhere('te.p1_by_p0 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP1TrackingSedangP2()
    {
        return $this->render('list-p1-tracking-sedang-p2', [
            'title' => 'Tracking Tiket yang <span class="text-grayest">Sedang dikerjakan oleh P2</span>',
        ]);
    }

    public function actionDatatablesP1RiwayatEskalasi()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NOT NULL')
                ->andWhere('te.due_date_by_p2 IS NOT NULL')
                ->andWhere('te.finish_date_p2 IS NOT NULL')
                ->andWhere('te.p1_by_p0 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP1RiwayatEskalasi()
    {
        return $this->render('list-p1-riwayat-eskalasi', [
            'title' => 'Riwayat Tiket yang <span class="text-grayest">Selesai Melalui Eskalasi</span>',
        ]);
    }

    // P2
    public function actionDatatablesP2RiwayatEskalasi()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('INNER JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NOT NULL')
                ->andWhere('te.due_date_by_p2 IS NOT NULL')
                ->andWhere('te.finish_date_p2 IS NOT NULL')
                ->andWhere('te.p2_by_p1 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionP2RiwayatEskalasi()
    {
        return $this->render('list-p2-riwayat-eskalasi', [
            'title' => 'Riwayat Tiket yang <span class="text-grayest">Selesai Melalui Eskalasi</span>',
        ]);
    }

    public function actionDatatablesAssignmentBelumP2()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('LEFT JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NULL')
                ->andWhere('te.p2_by_p1 = :id_department', [':id_department' => \Yii::$app->user->identity->user->id_department]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionAssignmentBelumP2($id=null)
    {
        // view all data
        if (!$id) {
            return $this->render('list-assignment-belum-p2', [
                'title' => 'Tiket Assignment & <span class="text-dark">Belum Dikerjakan oleh P2</span>',
            ]);
        }

        $model = [
            'tamu' => $this->findModel($id),
            'tiket_eskalasi' => TiketEskalasi::findOne($id),
            'accept_tiket' => 0,
        ];
        if ($model['tamu']->getTableType() != 1)
            throw new \yii\web\HttpException('403', 'Tiket dengan request '.$model['tamu']->no_request.' bukan merupakan tiket Eskalasi');
        else if ($model['tiket_eskalasi']->getStateStatus()[0] != 6)
            throw new \yii\web\HttpException('403', $model['tiket_eskalasi']->getStateStatus()[1]);

        $post = Yii::$app->request->post();

        if ($model['tiket_eskalasi']->load($post)) {
            $model['accept_tiket'] = $post['AcceptTiket'];
            switch ($model['accept_tiket']) {
                case 1:
                    $model['tiket_eskalasi']->accepted_by_p2 = \Yii::$app->user->identity->user->username;
                    $model['tiket_eskalasi']->accepted_time_p2 = new \yii\db\Expression("now()");
                    break;
                case 2:
                    $model['tiket_eskalasi']->p2_by_p1 = null;
                    $model['tiket_eskalasi']->reject_time_p2 = new \yii\db\Expression("now()");
                    $model['tiket_eskalasi']->reject_by_p2 = \Yii::$app->user->identity->user->username;
                    break;
            }
            if ($model['tiket_eskalasi']->save()) {
                if ($model['accept_tiket'] == 1) {
                    if (\Yii::$app->params['sms']) {
                        $message =
                            'KEMNAKER : Permohonan pelayanan dengan Nomor TIKET '.
                            $model['tiket_eskalasi']->no_request .
                            ' akan selesai pada Tanggal '.
                            TiketEskalasi::findOne($id)->due_date_by_p2 .
                            '. Terima kasih.';
                        $this->sms($message, $model['tamu']->no_hp);
                    }
                }
                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-assignment-belum-p2', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('form-assignment-belum-p2', [
                'model' => $model,
            ]);
        }
    }

    public function actionDatatablesAssignmentSedangP2()
    {
        $db = Tamu::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('tamu t')
                ->join('LEFT JOIN', 'tiket_eskalasi te', 't.no_request = te.no_request')
                ->where('t.accepted_by_p0 IS NOT NULL')
                ->andWhere('te.p1_by_p0 IS NOT NULL')
                ->andWhere('te.accepted_by_p1 IS NOT NULL')
                ->andWhere('te.p2_by_p1 IS NOT NULL')
                ->andWhere('te.accepted_by_p2 IS NOT NULL')
                ->andWhere('te.due_date_by_p2 IS NOT NULL')
                ->andWhere('te.finish_date_p2 IS NULL')
                ->andWhere('te.p2_by_p1 = :user_id_department', [':user_id_department' => \Yii::$app->user->identity->user->id_department])
                ->andWhere('te.accepted_by_p2 = :user_username', [':user_username' => \Yii::$app->user->identity->user->username]);
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                't.no_request',
                'nik',
                'nama',
                'keperluan',
            ]);
            // Yii::$app->d->ddx($query->createCommand()->sql);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
    public function actionAssignmentSedangP2($id=null)
    {
        // view all data
        if (!$id) {
            return $this->render('list-assignment-sedang-p2', [
                'title' => 'Tiket Assignment & <span class="text-dark">Sedang Dikerjakan oleh P2</span>',
            ]);
        }

        $model = [
            'tamu' => $this->findModel($id),
            'tiket_eskalasi' => TiketEskalasi::findOne($id),
        ];

        if ($model['tamu']->getTableType() != 1)
            throw new \yii\web\HttpException('403', 'Tiket dengan request '.$model['tamu']->no_request.' bukan merupakan tiket Eskalasi');
        else if ($model['tiket_eskalasi']->getStateStatus()[0] != 8)
            throw new \yii\web\HttpException('403', $model['tiket_eskalasi']->getStateStatus()[1]);

        $post = Yii::$app->request->post();

        if ($model['tiket_eskalasi']->load($post)) {
            $model['tiket_eskalasi']->finish_date_p2 = new \yii\db\Expression("now()");

            if ($model['tiket_eskalasi']->save()) {
                if (\Yii::$app->params['sms']) {
                    if (isset($model['tiket_eskalasi']->finish_file_p2))
                        $message =
                            'KEMNAKER : Permohonan pelayanan dengan Nomor TIKET '.
                            $model['tiket_eskalasi']->no_request .
                            ' telah selesai di proses. Mohon periksa E-mail Anda. Terima kasih.';
                    else
                        $message =
                            'KEMNAKER : Permohonan pelayanan dengan Nomor TIKET '.
                            $model['tiket_eskalasi']->no_request .
                            ' telah selesai di proses. Silahkan datang kembali ke PTSA KEMNAKER. Terima kasih.';
                    $this->sms($message, $model['tamu']->no_hp);
                }

                return $this->redirect(['view', 'id' => $model['tamu']->no_request]);
            }
            else {
                return $this->render('form-assignment-sedang-p2', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('form-assignment-sedang-p2', [
                'model' => $model,
            ]);
        }
    }

    protected function findModel($id)
    {
        if (($model = Tamu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function sms($message=null, $msisdn=null)
    {
        if ($message == null || $msisdn == null)
            return false;

        $u='klanyc';
        $p='Mjtqs123';
        $url = 'https://alpha.zenziva.net/apps/smsapi.php';
        
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$u.'&passkey='.$p.'&nohp='.$msisdn.'&pesan='.urlencode($message));
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        $results = curl_exec($curlHandle);
        curl_close($curlHandle);
        
        $result = explode(' +', $results);
        //$result = preg_split('/ +/', $results);
        if (!isset($result[3])) return false;
        return $result[3] == 0;
    }

    public function actionLoad($nik = null)
    {
        if (!$nik) return false;

        $soapUrl = "http://118.97.79.27:8000/Webservice/kemenaker/kemenaker.svc?wsdl"; // asmx URL of WSDL
        
        $xml_post_string =
        '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
            <Body>
                <CallNIKVerification xmlns="http://tempuri.org/">
                    <UserName>007320111sitium</UserName>
                    <password>a3NMG7AN</password>
                    <NIK>'.$nik.'</NIK>
                </CallNIKVerification>
            </Body>
        </Envelope>'; // xml post structure

        $headers = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "Accept: text/xml",
            "Cache-Control: no-cache",
            "Pragm no-cache",
            "SOAPAction: http://tempuri.org/Iwbektpdata/CallNIKVerification",
            "Content-length: ".strlen($xml_post_string),
        ); //SOAPAction: your op URL

        $url = $soapUrl;
        $soapUser="007320111sitium";
        $soapPassword="a3NMG7AN";
        // PHP cURL  for https connection with auth
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_USERPWD, $soapUser.":".$soapPassword); // username and password - declared at the top of the doc
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($ch);
        curl_close($ch);
        
        $r = str_replace("s:", "", $response);
        $r = str_replace("a:", "", $r);
        $r = str_replace("i:", "", $r);
        $r = simplexml_load_string($r);

        $r = $r->Body->CallNIKVerificationResponse->CallNIKVerificationResult;
        
        $data = [
            'nik' => $r->nilaiNIK->__toString(),
            'nama' => $r->nilainamaLengkap->__toString(),
            'no_kartu_keluarga' => $r->nilaiNoKK->__toString(),
            'tempat_lahir' => $r->nilaitempatLahir->__toString(),
            'tanggal_lahir' => $r->nilaitanggalLahir->__toString(),
            'jenis_kelamin' => $r->nilaijenisKelamin->__toString(),
            'golongan_darah' => $r->nilaiGolDarah->__toString(),
            'alamat' => $r->nilaiAlamat->__toString(),
            'rt' => $r->nilaiRT->__toString(),
            'rw' => $r->nilaiRW->__toString(),
            'desa_kelurahan' => $r->nilaiNamaKel->__toString(),
            'kecamatan' => $r->nilaiNamaKec->__toString(),
            'kota_kabupaten' => $r->nilaiNamaKab->__toString(),
            'provinsi' => $r->nilaiNamaProp->__toString(),
            'agama' => $r->nilaiAgama->__toString(),
            'pekerjaan' => $r->nilaijenisPekerjaan->__toString(),
        ];
        return $this->json($data);
    }

    public function actionLoadVpn($nik = null)
    {
        
        if (!$nik) return false;
            $soapUrl = "http://192.168.212.212/depnaker/server/";

        $passing_data = array(
            "userid" => "ptsaadm",
            "password" => "b@ngs1s",
            "nik" => $nik
        );
        $xml_post_string =  http_build_query($passing_data);
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $soapUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
         'Content-Type: application/x-www-form-urlencoded',
         'Content-Length: '.strlen($xml_post_string)
        ));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
        $response = curl_exec($ch);
        curl_close($ch);

        return $this->json(json_decode($response));
    }

    public function actionLoadReader($noRequest = null)
    {
        if (!$noRequest) return $this->json(0);

        if (($model = TamuCard::findOne($noRequest)) !== null) {
            $data = [
                'nik' => $model->nik,
                'nama' => $model->nama,
                'no_kartu_keluarga' => $model->no_kartu_keluarga,
                'tempat_lahir' => $model->tempat_lahir,
                'tanggal_lahir' => $model->tanggal_lahir,
                'jenis_kelamin' => $model->jenis_kelamin,
                'golongan_darah' => $model->golongan_darah,
                'alamat' => $model->alamat,
                'rt' => $model->rt,
                'rw' => $model->rw,
                'desa_kelurahan' => $model->desa_kelurahan,
                'kecamatan' => $model->kecamatan,
                'kota_kabupaten' => $model->kota_kabupaten,
                'provinsi' => $model->provinsi,
                'agama' => $model->agama,
                'pekerjaan' => $model->pekerjaan,
            ];
            return $this->json($data);
        } else {
            return $this->json(0);
        }
    }
}
