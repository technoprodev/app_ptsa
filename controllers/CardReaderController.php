<?php
namespace app_ptsa\controllers;

use Yii;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class CardReaderController extends Controller
{
    public function actionGetNoTiket($user, $pass) {
        if($user == 'astp' && $pass == '1w3r5')
            return json($this->sequence('tamu-no_request'));
        else
            throw new \yii\web\HttpException(403, 'You are forbidden from accessing this page.');
    }
}