<?php
namespace app_ptsa\controllers;

use Yii;
use yii\web\Controller;
use app_ptsa\models\Poll;

class PollingController extends Controller
{
    public function actionIndex($value='')
    {
        $this->layout = 'poll';

        $model = new Poll();
        if ($value != '') {
            $model->value = $value;
            $model->id_department = \Yii::$app->user->identity->user->ditjen->id;
            $model->save();
            Yii::$app->session->setFlash('poll-success', "Terima Kasih Atas Partisipasi Anda.");
            return $this->redirect(['index']);
        }
        else
            return $this->render('poll');
    }
}