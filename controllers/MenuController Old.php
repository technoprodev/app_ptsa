<?php
namespace app_ptsa\controllers;

use Yii;
use common\models\Menu;
use yii\db\Query;

class MenuController extends \common\yii\web\Controller
{
	public static $permissions = [
        'create', 'update', 'view', 'delete'
    ];

    public function behaviors() {
        return [
            'verbs' => $this->verbs([
                'delete' => ['post'],
                'position' => ['post'],
                'status' => ['post'],
            ]),
            'access' => $this->access([
                [['index', 'view'], 'view'],
                [['create'], 'create'],
                [['update', 'status', 'position'], 'update'],
                [['index', 'delete'], 'delete'],
            ]),
        ];
    }
		
	/**
	 * Lists all data.
	 * @return mixed
	 */
	public function actionIndex() {	
		Menu::reorder();

		return $this->render('index', [
			'query' => Menu::select('order by r.order_path'),
		]);
	}

	/**
	 * Creates a new data.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate($parent = null) {
		$model = new Menu();

		if ($model->load(\Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['index']);
		} 
		else {
			return $this->render('create', [
				'model' => $model,
				'parent' => $parent,
			]);
		}
	}

	/**
     * Updates an existing data.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if (\Yii::$app->request->isPost) {
        	$post = \Yii::$app->request->post();

        	// if parent updated, put order to last
	        if ($model->parent!=$post["Menu"]["parent"])
	        	$model->order = (new Query)->from("menu")
				    				->select('max("order")+1')
				    				->where(["parent" => !empty($post["Menu"]["parent"]) ? $post["Menu"]["parent"] : null])
				    				->scalar();

        	$model->load($post);
        	$model->save();

            return $this->redirect(['index']);
        } 
        else
        	return $this->render('update', [
				'model' => $model,
			]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

	/**
	 * change menu status (enable : 1|disable : 0)
	 * @param integer $id
	 * @return mixed
	 */
	public function actionStatus($id) {
		$model = $this->findModel($id);
		$model->enable = !$model->enable;
		$model->save();

		return $this->redirect(['index']);
	}

	/**
	 * update menu position
	 * ajax request only
	 */
	public function actionPosition() {
		if (!\Yii::$app->request->isAjax)
			return $this->redirect(['index']);
		
		$post = \Yii::$app->request->post();
		$this->findModel($post['id'])->updatePosition($post['order'], $post['parent']);
		exit;
	}

	/**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
