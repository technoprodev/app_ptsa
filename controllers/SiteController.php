<?php
namespace app_ptsa\controllers;

use Yii;
use technosmart\controllers\SiteController as SiteControl;
use technosmart\models\LoginForm;

class SiteController extends SiteControl
{
    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index', 'login', 'error'], true, ['?', '@'], ['GET', 'POST']],
                [['logout'], true, ['@'], ['POST']],
            ]),
        ];
    }
}