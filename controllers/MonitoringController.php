<?php
namespace app_ptsa\controllers;

use Yii;
use technosmart\yii\web\Controller;
use app_ptsa\models\Monitoring;

class MonitoringController extends Controller
{
    public static $permissions = [
        ['admin', 'Monitoring Admin'], ['ditjen', 'Monitoring Ditjen'], ['poll', 'Monitoring Hasil Poll'],
    ];

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'access' => $this->access([
                [['admin'], 'admin'],
                [['ditjen'], 'ditjen'],
                [['poll'], 'poll'],
            ])
        ]);
    }
        
    public function actionAdmin()
    {
        $tanggalMulai = null;
        $bulanMulai = null;
        $tahunMulai = null;
        $tanggalAkhir = null;
        $bulanAkhir = null;
        $tahunAkhir = null;

        if($post = Yii::$app->request->post()) {
            $bulanMulai = $post['Monitoring']['bulanMulai'];
            $tahunMulai = $post['Monitoring']['tahunMulai'];
            $bulanAkhir = $post['Monitoring']['bulanAkhir'];
            $tahunAkhir = $post['Monitoring']['tahunAkhir'];
        }

        if (!$tanggalMulai) $tanggalMulai = '01';
        if (!$bulanMulai) $bulanMulai = '01';
        if (!$tahunMulai) $tahunMulai = date('Y');

        if (!$tanggalAkhir) $tanggalAkhir = '30';
        if (!$bulanAkhir) $bulanAkhir = date('m');
        if ($bulanAkhir == 2) $tanggalAkhir = '28';
        if (!$tahunAkhir) $tahunAkhir = date('Y');

        $waktuMulai = $tahunMulai . '-' . $bulanMulai . '-' . $tanggalMulai;
        $waktuAkhir = $tahunAkhir . '-' . $bulanAkhir . '-' . $tanggalAkhir;

        $bind = [
            // 'waktuMulai' => $waktuMulai,
            // 'waktuAkhir' => $waktuAkhir,
            // 'waktuMulai' => new \yii\db\Expression($waktuMulai),
            // 'waktuAkhir' => new \yii\db\Expression($waktuAkhir),
        ];

        $data_kinerja = \Yii::$app->dba->createCommand(
            "SELECT
                CONCAT(COALESCE(d.alias, ''), ' - ', d.name) AS name,
                count(te.no_request) AS tiket_eskalasi,
                count(tm.no_request) AS tiket_mediasi,
                count(t.no_request) - (count(te_full.no_request)+count(tm.no_request)) AS tiket_selesai
            FROM
                (SELECT * FROM tamu  WHERE
                accepted_time_p0 BETWEEN DATE '" . $waktuMulai . "' AND DATE '" . $waktuAkhir . "') t
            INNER JOIN user u ON t.accepted_by_p0 = u.username
            RIGHT JOIN department d ON u.id_department = d.id
            LEFT JOIN
                (SELECT * FROM tiket_eskalasi WHERE finish_date_p2 IS NOT NULL)
                te ON t.no_request = te.no_request
            LEFT JOIN tiket_eskalasi te_full ON t.no_request = te_full.no_request
            LEFT JOIN tiket_mediasi tm ON t.no_request = tm.no_request
            WHERE
                d.parent IS NULL
            GROUP BY d.id, d.name", $bind
        )->queryAll();

        $data_count_kinerja = [
            'tiket_eskalasi' => 0,
            'tiket_mediasi' => 0,
            'tiket_selesai' => 0,
        ];
        foreach ($data_kinerja as $key => $value) {
            $data_count_kinerja['tiket_eskalasi'] += $value['tiket_eskalasi'];
            $data_count_kinerja['tiket_mediasi'] += $value['tiket_mediasi'];
            $data_count_kinerja['tiket_selesai'] += $value['tiket_selesai'];
        }

        $model['monitoring'] = new Monitoring();
        $model['monitoring']->bulanMulai = $bulanMulai;
        $model['monitoring']->tahunMulai = $tahunMulai;
        $model['monitoring']->bulanAkhir = $bulanAkhir;
        $model['monitoring']->tahunAkhir = $tahunAkhir;

        return $this->render('monitoring-admin', [
            'data_kinerja' => $data_kinerja,
            'data_count_kinerja' => $data_count_kinerja,
            'model' => $model,
            'title' => 'Monitoring Kinerja Semua Ditjen',
            'waktuMulai' => $waktuMulai,
            'waktuAkhir' => $waktuAkhir,
        ]);
    }

    public function actionDitjen()
    {
        $tanggalMulai = null;
        $bulanMulai = null;
        $tahunMulai = null;
        $tanggalAkhir = null;
        $bulanAkhir = null;
        $tahunAkhir = null;

        if($post = Yii::$app->request->post()) {
            $bulanMulai = $post['Monitoring']['bulanMulai'];
            $tahunMulai = $post['Monitoring']['tahunMulai'];
            $bulanAkhir = $post['Monitoring']['bulanAkhir'];
            $tahunAkhir = $post['Monitoring']['tahunAkhir'];
        }

        if (!$tanggalMulai) $tanggalMulai = '01';
        if (!$bulanMulai) $bulanMulai = '01';
        if (!$tahunMulai) $tahunMulai = date('Y');

        if (!$tanggalAkhir) $tanggalAkhir = '30';
        if (!$bulanAkhir) $bulanAkhir = date('m');
        if ($bulanAkhir == 2) $tanggalAkhir = '28';
        if (!$tahunAkhir) $tahunAkhir = date('Y');

        $waktuMulai = $tahunMulai . '-' . $bulanMulai . '-' . $tanggalMulai;
        $waktuAkhir = $tahunAkhir . '-' . $bulanAkhir . '-' . $tanggalAkhir;

        $bind = [
            // 'waktuMulai' => $waktuMulai,
            // 'waktuAkhir' => $waktuAkhir,
            // 'waktuMulai' => new \yii\db\Expression($waktuMulai),
            // 'waktuAkhir' => new \yii\db\Expression($waktuAkhir),
            'userDepartment' => \Yii::$app->user->identity->user->id_department,
        ];

        $data_kinerja = \Yii::$app->dba->createCommand(
            "SELECT
                CONCAT(COALESCE(d.alias, ''), ' - ', d.name) AS name,
                count(te.no_request) AS tiket_eskalasi,
                count(tm.no_request) AS tiket_mediasi,
                count(t.no_request) - (count(te_full.no_request)+count(tm.no_request)) AS tiket_selesai
            FROM
                (SELECT * FROM tamu  WHERE
                accepted_time_p0 BETWEEN DATE '" . $waktuMulai . "' AND DATE '" . $waktuAkhir . "') t
            INNER JOIN user u ON t.accepted_by_p0 = u.username
            RIGHT JOIN department d ON u.id_department = d.id
            LEFT JOIN
                (SELECT * FROM tiket_eskalasi WHERE finish_date_p2 IS NOT NULL)
                te ON t.no_request = te.no_request
            LEFT JOIN tiket_eskalasi te_full ON t.no_request = te_full.no_request
            LEFT JOIN tiket_mediasi tm ON t.no_request = tm.no_request
            WHERE
                d.parent IS NULL AND
                d.id = :userDepartment
            GROUP BY d.id, d.name", $bind
        )->queryOne();

        $model['monitoring'] = new Monitoring();
        $model['monitoring']->bulanMulai = $bulanMulai;
        $model['monitoring']->tahunMulai = $tahunMulai;
        $model['monitoring']->bulanAkhir = $bulanAkhir;
        $model['monitoring']->tahunAkhir = $tahunAkhir;

        return $this->render('monitoring-ditjen', [
            'data_kinerja' => $data_kinerja,
            'model' => $model,
            'title' => 'Monitoring Kinerja ' . \Yii::$app->user->identity->user->department->name,
            'waktuMulai' => $waktuMulai,
            'waktuAkhir' => $waktuAkhir,
        ]);
    }

    public function actionPoll()
    {
        $tanggalMulai = null;
        $bulanMulai = null;
        $tahunMulai = null;
        $tanggalAkhir = null;
        $bulanAkhir = null;
        $tahunAkhir = null;

        if($post = Yii::$app->request->post()) {
            $bulanMulai = $post['Monitoring']['bulanMulai'];
            $tahunMulai = $post['Monitoring']['tahunMulai'];
            $bulanAkhir = $post['Monitoring']['bulanAkhir'];
            $tahunAkhir = $post['Monitoring']['tahunAkhir'];
        }

        if (!$tanggalMulai) $tanggalMulai = '01';
        if (!$bulanMulai) $bulanMulai = '01';
        if (!$tahunMulai) $tahunMulai = date('Y');

        if (!$tanggalAkhir) $tanggalAkhir = '30';
        if (!$bulanAkhir) $bulanAkhir = date('m');
        if ($bulanAkhir == 2) $tanggalAkhir = '28';
        if (!$tahunAkhir) $tahunAkhir = date('Y');

        $waktuMulai = $tahunMulai . '-' . $bulanMulai . '-' . $tanggalMulai;
        $waktuAkhir = $tahunAkhir . '-' . $bulanAkhir . '-' . $tanggalAkhir;

        $bind = [
            // 'waktuMulai' => $waktuMulai,
            // 'waktuAkhir' => $waktuAkhir,
            // 'waktuMulai' => new \yii\db\Expression($waktuMulai),
            // 'waktuAkhir' => new \yii\db\Expression($waktuAkhir),
        ];

        $dataPoll = \Yii::$app->dba->createCommand(
            "select
              IFNULL(CONCAT(COALESCE(d.alias, ''), ' - ', d.name), '(umum)') as name,
              count(case when value = 0 then VALUE end) '0',
              count(case when value = 1 then VALUE end) '1',
              count(case when value = 2 then VALUE end) '2'
            from poll p
            left join department d on p.id_department = d.id
            where p.created_at between DATE '" . $waktuMulai . "' AND DATE '" . $waktuAkhir . "'
            group by d.id
            order by d.id", $bind
        )->queryAll();

        $model['monitoring'] = new Monitoring();
        $model['monitoring']->bulanMulai = $bulanMulai;
        $model['monitoring']->tahunMulai = $tahunMulai;
        $model['monitoring']->bulanAkhir = $bulanAkhir;
        $model['monitoring']->tahunAkhir = $tahunAkhir;

        $dataCountPoll = [
            0 => 0,
            1 => 0,
            2 => 0,
        ];
        foreach ($dataPoll as $key => $value) {
            $dataCountPoll[0] += $value['0'];
            $dataCountPoll[1] += $value['1'];
            $dataCountPoll[2] += $value['2'];
        }

        return $this->render('monitoring-poll', [
            'dataPoll' => $dataPoll,
            'dataCountPoll' => $dataCountPoll,
            'model' => $model,
            'title' => 'Monitoring Polling Kepuasan Pelanggan',
            'waktuMulai' => $waktuMulai,
            'waktuAkhir' => $waktuAkhir,
        ]);
    }
}