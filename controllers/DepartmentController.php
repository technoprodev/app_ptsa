<?php
namespace app_ptsa\controllers;

use Yii;
use app_ptsa\models\Department;
use technosmart\yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DepartmentController implements the CRUD actions for Department model.
 */
class DepartmentController extends Controller
{
    public static $permissions = [
        'view', 'create', 'update', 'delete'
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['view'], 'view'],
                [[
                    'index',
                    'datatables',
                ], 'view'],
                [[
                    'index',
                    'datatables',
                    'form',
                ], 'create'],
                [[
                    'index',
                    'datatables',
                    'form',
                ], 'create'],
                [[
                    'index',
                    'datatables',
                    'delete',
                ], 'delete'],
            ]),
        ];
    }

    public function actionDatatables()
    {
        $db = Department::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('department');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'alias',
                'name',
                'description',
                'parent',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // only nulled all params or valued all params allowed
        $nulledParams = $valuedParams = false;
        foreach (func_get_args() as $key => $value) {
            if (is_null($value))
                $nulledParams = true;
            else
                $valuedParams = true;
        }
        if ($nulledParams && $valuedParams)
            throw new \yii\web\HttpException(404, "Page not found.");

        // view all data
        if ($nulledParams) {
            return $this->render('list', [
                'title' => 'Department',
            ]);
        }
        // view single data
        else {
            $model['department'] = $this->findModel($id);
            return $this->render('one', [
                'model' => $model,
                'title' => 'Department',
            ]);
        }
    }

    /**
     * If param(s) is null, creates new data(s) from model(s).
     * If all param(s) is not null, updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionForm($id = null)
    {
        $render = true;

        // only nulled all params or valued all params allowed
        $nulledParams = $valuedParams = false;
        foreach (func_get_args() as $key => $value) {
            if (is_null($value))
                $nulledParams = true;
            else
                $valuedParams = true;
        }
        if ($nulledParams && $valuedParams)
            throw new \yii\web\HttpException(404, "Page not found.");

        // init all models
        $model['department'] = $nulledParams ? new Department() : $this->findModel($id);

        // init value if new request
        if (Yii::$app->request->isGet && $model['department']->isNewRecord) {
        }
        // save all models & serve ajax validation
        else if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['department']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['department'])
                );
                return $this->json($result);
            }

            // models modification goes here

            if ($model['department']->validate()) {
                if ($model['department']->save(false)) {
                    $render = false;
                }
            }
        }

        // if new request / if saving all models fails
        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Department',
            ]);
        // if saving all models success
        else
            return $this->redirect(['index', 'id' => $model['department']->id]);
    }

    /**
     * Deletes an existing Department model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Department model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Department the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Department::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
