<?php
namespace app_ptsa\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
	public $sourcePath = '@app_ptsa/assets';

    public $js = [
        'js/app.js',
    ];

    public $depends = [
        'technosmart\assets_manager\VueAsset',
        'technosmart\assets_manager\RequiredAsset',
    ];
}