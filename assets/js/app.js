if(typeof Vue == 'function') {
    if(typeof VueDefaultValue == 'object') {
        Vue.use(VueDefaultValue);
    }

    if(typeof Vue.http == 'function') {
        Vue.http.headers.common['X-CSRF-Token'] = csrfToken;
    }

    Vue.directive('init', {
        inserted: function(el) {
            pluginInit(el);
        },
        componentUpdated: function(el) {
            pluginInit(el);
        },
    });

    var vm = new Vue({
        el: '#app',
        data: {
            tamu: {
                no_request: null,
                nik: null,
                nama: null,
                no_kartu_keluarga: null,
                tempat_lahir: null,
                tanggal_lahir: null,
                jenis_kelamin: null,
                golongan_darah: null,
                alamat: null,
                rt: null,
                rw: null,
                desa_kelurahan: null,
                kecamatan: null,
                kota_kabupaten: null,
                provinsi: null,
                agama: null,
                pekerjaan: null,
                instansi: null,
                keperluan: null,
                no_hp: null,
                comment_by_p0: null
            },
            tiket_eskalasi: {
                no_request: null,
                p1_by_p0: null,
                reassign_time_p0: null,
                accepted_by_p1: null,
                accepted_time_p1: null,
                comment_by_p1: null,
                p2_by_p1: null,
                reassign_time_p1: null,
                reject_by_p1: null,
                reject_time_p1: null,
                reject_comment_by_p1: null,
                accepted_by_p2: null,
                accepted_time_p2: null,
                comment_by_p2: null,
                due_date_by_p2: null,
                reject_by_p2: null,
                reject_time_p2: null,
                reject_comment_by_p2: null,
                finish_comment_p2: null,
                finish_date_p2: null,
                finish_file_p2: null,
                cancel_by_p0: null,
                cancel_time_p0: null,
                cancel_comment_p0: null,
            },
            tiket_mediasi: {
                pic: null,
                no_hp: null
            },
            tipe_tiket: 0,
            // 1 eskalasi
            // 2 mediasi
            // 3 selesai_di_tempat
            accept_tiket: 0,
            // 1 accept
            // 2 reject
        },
        /*watch: {
            'tamu.tanggal_lahir': function(newVal, oldVal){
                console.log(oldVal + ' becomes ' + newVal);
            }
        },*/
        methods: {
            setTipeTiket : function(tipe_tiket){
                this.tipe_tiket = tipe_tiket;
            },
            setAcceptTiket : function(accept_tiket){
                this.accept_tiket = accept_tiket;
            },
            loadReader : function(){
                this.$http
                    .post(
                        fn.urlTo('tamu/load-reader', {noRequest : this.tamu.no_request})
                    )
                    .then(function(response) {
                        response = response['body'];
                        if(response == 0) 
                            return fn.alert('Not Found', 'No Request <strong>' + this.tamu.no_request + '</strong> tidak ditemukan pada database', 'warning');
                        else {
                            fn.alert('Data Found', 'No Request <strong>' + this.tamu.no_request + '</strong> berhasil ditemukan pada database', 'success')
                            this.tamu.nik = response['nik'];
                            this.tamu.nama = response['nama'];
                            this.tamu.no_kartu_keluarga = response['no_kartu_keluarga'];
                            this.tamu.tempat_lahir = response['tempat_lahir'];
                            this.tamu.tanggal_lahir = response['tanggal_lahir'];
                            this.tamu.jenis_kelamin = response['jenis_kelamin'];
                            this.tamu.golongan_darah = response['golongan_darah'];
                            this.tamu.alamat = response['alamat'];
                            this.tamu.rt = response['rt'];
                            this.tamu.rw = response['rw'];
                            this.tamu.desa_kelurahan = response['desa_kelurahan'];
                            this.tamu.kecamatan = response['kecamatan'];
                            this.tamu.kota_kabupaten = response['kota_kabupaten'];
                            this.tamu.provinsi = response['provinsi'];
                            this.tamu.agama = response['agama'];
                            this.tamu.pekerjaan = response['pekerjaan'];
                        }
                    }, function(error) {
                        return fn.alert('Service Unavailable', 'Harap hubungi administrator untuk menyelesaikan masalah ini', 'danger');
                    });
            },
            loadWebService : function(){
                this.$http
                    .post(
                        fn.urlTo('tamu/load-vpn', {nik : this.tamu.nik})
                    )
                    .then(function(response) {
                        response = response['body'];
                        if(response['STATUS'] == '0')
                            return fn.alert('Not Found', 'NIK <strong>' + this.tamu.nik + '</strong> tidak ditemukan pada database', 'warning');
                        else if (response['STATUS'] == '1'){
                            fn.alert('Data Found', 'NIK <strong>' + this.tamu.nik + '</strong> berhasil ditemukan pada database', 'success')
                            this.tamu.nik = response['NIK'];
                            this.tamu.nama = response['NAMA_LGKP'];
                            this.tamu.no_kartu_keluarga = response['NO_KK'];
                            this.tamu.tempat_lahir = response['TMPT_LHR'];
                            this.tamu.tanggal_lahir = response['TGL_LHR'];
                            this.tamu.jenis_kelamin = response['JENIS_KLMIN'];
                            this.tamu.golongan_darah = response['GOL_DARAH'];
                            this.tamu.alamat = response['ALAMAT'];
                            this.tamu.rt = response['NO_RT'];
                            this.tamu.rw = response['NO_RW'];
                            this.tamu.desa_kelurahan = response['KEL_NAME'];
                            this.tamu.kecamatan = response['KEC_NAME'];
                            this.tamu.kota_kabupaten = response['KAB_NAME'];
                            this.tamu.provinsi = response['PROP_NAME'];
                            this.tamu.agama = response['AGAMA'];
                            this.tamu.pekerjaan = response['JENIS_PKRJN'];
                        }
                    }, function(error) {
                        return fn.alert('Service Unavailable', 'Harap hubungi administrator untuk menyelesaikan masalah ini', 'danger');
                    });
            },
        }
    });
}